<?

$fail=0;

require_once('mysql.php');

require_once('html.php');

require_once('xmlrpc.inc');

$query="SELECT 	name, numbers, id,
		call_label, cancel_label, disconnect_label,
		progress_label, preset, widget_width,
		widget_height, button_width, button_height,i_account
	FROM 	buttons 
	WHERE  	id=".get_par('id');
			
$res=mysql_query($query) or die(mysql_error());

if($row=mysql_fetch_row($res)) {
	$name		=$row[0];
	$numbers	=$row[1];
	$id		=$row[2];
	$call_label	=$row[3];
	$cancel_label	=$row[4];
	$disconnect_label=$row[5];
	$progress_label	=$row[6];
	$preset		=$row[7];
	$widget_width	=$row[8];
	$widget_height	=$row[9];
	$button_width	=$row[10];
	$button_height	=$row[11];
	$i_account	=$row[12];
}

$params = array(new xmlrpcval(array("i_account"  => new xmlrpcval("$i_account", "int"),
	        ), 'struct'));
$msg = new xmlrpcmsg('getAccountInfo', $params);

$call_numbers = split(",", $numbers);
$rand_key = array_rand($call_numbers,1);
$call_number = $call_numbers[$rand_key];

/* replace here URL  and credentials to access to the API */

$cli = new xmlrpc_client('https://login.fasttalks.com/xmlapi/xmlapi');
$cli->setSSLVerifyPeer(false);
$cli->setCredentials(XML_LOGIN, XML_PASWORD, CURLAUTH_DIGEST);

$r = $cli->send($msg, 20);       /* 20 seconds timeout */

if ($r->faultCode()) {
	$error=1;
}

$success_fetch = $r->value();

$authname_s  = $success_fetch->structMem('authname');

$authname = $authname_s->scalarVal();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title></title>
	<meta name="title" content="" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    <!--[if lt IE 7]>
     <script defer type="text/javascript" src="script/pngfix.js"></script>
     <![endif]-->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" type="text/css" href="script/cusel.css" />
    <script type="text/javascript" src="script/jquery-1.4.2.js"></script>
    <script type="text/javascript" src="script/cusel-min-2.3.1.js"></script>
    <script type="text/javascript" src="script/select-settings.js"></script>      
    </head>
<body style="background-image: none;">
<script type="text/JavaScript">
function changeState(state)
{
if(state==1){$('#callBtn').val($('#callInput').val());}
if(state==2)
{
$('#establish').css('display','block');
$('#callBtn').val($('#cancelInput').val());
}
else{$('#establish').css('display','none');}
if(state==3){$('#callBtn').val($('#connectInput').val());}
}



/*var loSipProxy = '173.193.14.90';
var loSipRealm = '173.193.14.90';
 */
var loSipProxy = '174.36.24.14';
var loSipRealm = '174.36.24.14';

var loMyUserName  = '<? echo $_SESSION['authname'] = $authname; ?>';
var loMyPassword  = '123talk_fast_die_young';


var LS_REGISTERED      = 21000;
var LS_REGISTER_FAILED = 24000;
var LS_REGISTER_FAILED_NOT_CONNECT = LS_REGISTER_FAILED + 1;
var LS_REGISTER_FAILED_NOT_AUTHORIZED  = LS_REGISTER_FAILED + 2;
var LS_REGISTER_FAILED_TIMEOUT = LS_REGISTER_FAILED + 3;
var CS_REMOTE_ALERTING = 3000;
var CS_CONNECTED       = 4000;
var CS_DISCONNECTED    = 8000;
var CS_ALERTING        = 10000;
var CS_CAUSE_NORMAL    = 1;
var CS_CAUSE_BUSY      = 8;
var CS_CAUSE_CODEC_FAILURE = 21;
var APP_LOADED         = 1010;



var isLogged = 0;
var incomingCall = 0;
var allowLogin = 0;
var connected = false;

function Call() {
  if(!isLogged) {
    DisplayMessage('Not Logged in yet.', 2);
    return false;
  }

  DisplayMessage('<? echo $progress_label; ?>', 0);
  
  var button = document.getElementById('call_button');  
  button.value = "<? echo $cancel_label?>";
  button.onclick = Hangup;

  var applet = (document.webphone.length) ? document.webphone[1] : document.webphone;
  applet.Call('<? echo $call_number; ?>');
}

function Hangup() {
  var applet = (document.webphone.length) ? document.webphone[1] : document.webphone;
  applet.Hangup();
  var button = document.getElementById('call_button');
  button.value = "<? echo $call_label?>";
  button.onclick = Call;
}

function eventHandler(msg) {

  switch (parseInt(msg)) {

    case CS_CAUSE_NORMAL:
      DisplayMessage('Finished: ' + time_str(duration), 2);
      stopTimer();

      /*var gbut_hup = document.getElementById('gbut_hup');
      hup_pressed = true;
      gbut_up(gbut_hup, 'hup')
      */

      connected = false;

      break;

    case CS_CAUSE_CODEC_FAILURE:
      DisplayMessage('Media not supported', 2);

      /*var gbut_hup = document.getElementById('gbut_hup');
      hup_pressed = true;
      gbut_up(gbut_hup, 'hup')
      */

      connected = false;

      break;

    case CS_CAUSE_BUSY:
      DisplayMessage('Busy', 2);

      /*var gbut_hup = document.getElementById('gbut_hup');
      hup_pressed = true;
      gbut_up(gbut_hup, 'hup')
      */

      connected = false;

      break;

    case CS_DISCONNECTED:
      DisplayMessage('Finished: ' + time_str(duration), 2);
      stopTimer();

      /*var gbut_hup = document.getElementById('gbut_hup');
      hup_pressed = true;
      gbut_up(gbut_hup, 'hup')
      */

      connected = false;

      break;

    case CS_REMOTE_ALERTING:
    DisplayMessage('Calling', 0);

      break;

    case CS_CONNECTED:
      if (!connected) {
        DisplayMessage('Connected', 1);
        startTimer();
        incomingCall = 0;

	var button = document.getElementById('call_button');
	button.value = "<? echo $disconnect_label; ?>";
	button.onclick = Hangup;


        connected = true;
      }

      break;

    case APP_LOADED:
      allowLogin = 1;
      Login();

      break;
   case CS_ALERTING:
      incomingCall = 1;

      break;

    case LS_REGISTERED:
      if (!isLogged)
        DisplayMessage('Login Successful', 0);
      isLogged = 1;

      break;

    case LS_REGISTER_FAILED:
      DisplayMessage('Login Failed', 2);
      break;

    case LS_REGISTER_FAILED_NOT_AUTHORIZED:
      DisplayMessage('Unauthorized', 2);
      break;

    case LS_REGISTER_FAILED_TIMEOUT:
      DisplayMessage('Timeout', 2);
      break;

    case LS_REGISTER_FAILED_NOT_CONNECT:
      DisplayMessage('Connection Problem', 2);
      break;

    default:
      // DisplayMessage('Unknown event: ' + msg, 2);
      break;
  }
}


function Login() {
  if (!allowLogin){
    DisplayMessage('WebPhone is loading, please wait...', 2);
    return;
  }
  DisplayMessage('Trying to login...', 1);
  var applet = (document.webphone.length) ? document.webphone[1] : document.webphone;
  
  var sipserver = loSipProxy;
  var stun = null;
  var username = loMyUserName;
  var auth_user = loMyUserName;
  var password = loMyPassword;
  var proxy = null;
  var realm = loSipRealm;
  var useHA1 = false;
  applet.Login(username, auth_user, password, sipserver, proxy, realm, stun, useHA1);
}

var timer;
var duration = -1;

function startTimer() {
  duration++;
  DisplayMessage("Connected: " + time_str(duration), 1);
  timer = setTimeout("startTimer()", 1000);
}

function time_str(d) {
  d = Number(d);
  if (d < 0) d = 0;

  var hours = Math.floor(d / 3600);
  d = d - 3600 * hours;
  var mins = Math.floor(d / 60);
  d = d - 60 * mins;
  var secs = d;

  if (hours < 10) hours = "0" + hours;
  if (mins < 10) mins = "0" + mins;
  if (secs < 10) secs = "0" + secs;

  return (hours > 0 ? hours + ':' : '') + mins + ':' + secs;
}

function stopTimer() {
  clearTimeout(timer);
  duration = -1;
}
function DisplayMessage(s, color) {
  var gbar_status = document.getElementById('phone_status');

  if (color == 1) {
    gbar_status.style.color = "green";
  } else if (color == 2) {
    gbar_status.style.color = "red";
  } else {
    gbar_status.style.color = "black";
  }
  
   $('#phone_status').css('display','block');

  gbar_status.innerHTML = s;
}


</script>
<object name="webphone"
    classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93"
    codebase="https://java.sun.com/update/1.6.0/jinstall-6-windows-i586.cab#Version=6,0,0,0"
    width="1" height="1">
    <param name="java_version" value="1.5+">
    <param name="code" value="com.webphone.sip.WebSipPhone.class" />
    <param name="codebase" value="/webphone/" />
    <param name="archive" value="https://login.fasttalks.com/webphone/webphone-core.jar" />
    <param name="mayscript" value="true" />
	<param name=cprt value="Sippy Software, Inc" />
    <param name="separate_jvm" value="true" />
    <param name="webphone_subdir" value="/webphone" />
    <param name="webphone_domain" value="https://login.fasttalks.com" />
    <comment>   <!-- Non-WinIE Browsers -->
        <embed name="webphone"
            classid="java:com.webphone.sip.WebSipPhone.class"
            type="application/x-java-applet" width="1" height="1"
            pluginspage="http://java.sun.com/update/1.6.0/jre-6-windows-i586.xpi"
            java_version="1.5+"
            code="com.webphone.sip.WebSipPhone.class"
            codebase="/webphone/"
            archive="https://login.fasttalks.com/webphone/webphone-core.jar"
	    mayscript="true"
	    cprt="Sippy Software, Inc"
            separate_jvm="true"
            webphone_subdir="/webphone"
            webphone_domain="https://login.fasttalks.com">
        </embed>
    </comment>
</object>


	<table width="<? echo $widget_width; ?>" height="<? echo $widget_height; ?>" border="0">
  <tr>
  <td align="center" valign="middle"><input id=call_button  style="color:#FFF; border:1px; background:#<? echo $preset; ?>; font-size:18px; width:<? echo $button_width; ?>px; height:<? echo $button_height; ?>px" type="button" value="<? echo $call_label; ?>" id="callBtn" onClick="Call();" /><!-- button_design--></td>
    </tr>
    <tr>
    <td align="center" valign="middle"><div style="display:none" id="phone_status"><? echo $progress_label; ?></div></td>
  </tr>
</table>
 
</body>
</html>

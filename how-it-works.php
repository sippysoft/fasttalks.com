<? 
$title = "How It Works";

require_once('header.php'); ?>
	<div id="middle">



		<div id="container">

			<div id="contentHowItWorks">

				<div class="howItWorks_txt blue font_26">

                	How It Works

                </div><!-- howItWorks_txt-->

                <div class="steps">

                	<table width="660" border="0" cellspacing="0" cellpadding="0">

                      <tr>

                        <td width="7%"><img src="img/1.png" width="27" height="27" alt="1" /></td>

                        <td width="11%"><img src="img/hand_ico.png" alt="" width="40" height="79" /></td>

                        <td width="82%">Sign up for Call Us service.</td>

                      </tr>

                      <tr>

                        <td><img src="img/2.png" width="27" height="27" alt="2" /></td>

                        <td><img src="img/phone_setup_ico.png" alt="" width="51" height="59" /></td>

                        <td>In the control panel, set up the phone number you want clients to be directed to.<br /></td>

                      </tr>

                      <tr>

                        <td><img src="img/3.png" width="27" height="27" alt="3" /></td>

                        <td><img src="img/add_code_ico.png" alt="" width="51" height="68" /></td>

                        <td>Embed a Call Us button in your website by adding some simple HTML code.</td>

                      </tr>

                      <tr>

                        <td><img src="img/4.png" width="27" height="27" alt="4" /></td>

                        <td><img src="img/callusOnYourSite_ico.png" alt="" width="51" height="63" /></td>

                        <td>With just one mouse click, your website visitors can now place direct calls to your designated phone number. All they need is a microphone or headset.</td>

                      </tr>

                    </table>



                </div><!-- steps-->                            	

            <div class="clearfix"></div>    

		  </div><!-- #content-->

		</div><!-- #container-->       



	</div><!-- #middle-->

<? require_once('footer.php'); ?>

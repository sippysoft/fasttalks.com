	<div id="footer">
    	<div class="copyright blue">
        	Copyright 2011 FastTalks, Inc.
        </div><!-- copyright-->
    	<div class="bottomMenu">
        	<ul>
            	<li><a href="privacy-policy.php">Privacy Policy</a></li>
                <li><a href="terms-and-conditions.php">Terms &amp; Conditions</a></li>
                <li><a href="refund-policy.php">Refund Policy</a></li>
                <li><a href="contact-us.php">Contact Us</a></li>
            </ul>
        </div><!-- bottomMenu-->
    </div><!-- footer-->
</div><!-- #wrapper -->

</body>
</html>
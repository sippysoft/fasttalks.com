<? 
$title = "Contact Us";
require_once('header.php');
require_once('html.php');


$mail_to="info@fasttalks.com";

$contact_text="Thank you for your interest . Your information request is
important to us, so please take a moment to complete  the
form below and let us know how we can assist you.";

if(isset_par('send')) {
	if(strlen(get_par('name')) && strlen(get_par('request')) ) {
		mail($mail_to, "FastTalks contact form request", "
Hello,

There was a contact form request from FastTalks web site.
The details are as follows:

Name:".get_par('name')."
Company:".get_par('company')."
Email:".get_par('email')."
Phone:".get_par('phone')."
Request:".get_par('request')."
			");
		$contact_text="Thank you. Your request will be forwarded to the appropriate person. We will respond to you as soon as possible.";
	} else {
		$contact_text="Please fill in all required fields";
	}	
}


?>
	<div id="middle">

		<div id="container">
			<div id="content_contactUs">
				<div class="contactUs">
                	<table width="620" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="20%" height="65">&nbsp;</td>
                        <td width="80%" class="orange">
                        <div class="contactUs_txt"> <? echo $contact_text; ?> </div> </td>
		      </tr>
<form action="contact-us.php" method=post>
<input type=hidden name=send value=1>
                      <tr>
                        <td height="53">You name*:</td>
                        <td> 
                       	   <div class= "left-input"><div class= "right-input"><div class= "fill-input">
	                           <input  type="text" name="name"/>
                            </div></div></div>  </td>
                      </tr>
                      <tr>
                        <td height="51">Company</td>
                        <td>
                          <div class= "left-input"><div class= "right-input"><div class= "fill-input">
	                          <input  type="text" name="company" />
                           </div></div></div>  
                        </td>
                      </tr>
                      <tr>
                        <td height="53">Email address:</td>
                        <td>
   		                  <div class= "left-input"><div class= "right-input"><div class= "fill-input">
	                          <input  type="text" name="email" />
                           </div></div></div>                               
                        </td>
                      </tr>
                      <tr>
                        <td height="51">Phone number:</td>
                        <td>
                          <div class= "left-input"><div class= "right-input"><div class= "fill-input">
	                          <input  type="text" name="phone" />
                           </div></div></div>  
                        </td>
                      </tr>
                      <tr>
                        <td height="177">Request*:</td>
                        <td><textarea name="request" ></textarea></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td><span class="blue bold">* reguired fields</span>
                        <span class="padd_left"><input  type="image" src="img/send_button.png" /></span></td>
		      </tr>
</form>
                    </table>

                </div><!-- contactUs-->
		  </div><!-- #content-->
		</div><!-- #container--> 	

	</div><!-- #middle-->

<? require_once('footer.php');
?>

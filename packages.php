<?


$packages = array(

 0 => array(
 'name' => 'Personal - 29$/month',
 'i_billing_plan' => 11,
 'multi_button' => 0,
 'feature_list' => array ('Free','1 widget','1 line','Multiple numbers','Call to skype, SIP') 
 ),

 1 => array(
 'name' => 'Company - 59$/month',
 'i_billing_plan' => 12,
 'multi_button' => 1,
 'feature_list' => array ('Free','1 widget','1 line','Multiple numbers','Call to skype, SIP')
 ),

 2 => array(
 'name' => 'Enterprise - 199$/month',
 'i_billing_plan' => 13,
 'multi_button' => 1,
 'feature_list' => array ('Free','1 widget','1 line','Multiple numbers','Call to skype, SIP')
 ),
);

$tzs = array(
	25 => '(GMT -11:00) Samoa, American Samoa',
	351 => '(GMT -10:00) Hawaii, Papeete',
	347 => '(GMT -09:00) Alaska',
        88 => '(GMT -08:00) Pacific Time(US & Canada)',
        344 => '(GMT -07:00) Arizona',
        232 => '(GMT -07:00) Chihuahua, La Paz, Mazatlan',
        84 => '(GMT -07:00) Mountain Time (US & Canada)',
        79 => '(GMT -06:00) Central Time (US & Canda)',
        227 => '(GMT -06:00) Guadalajara, Mexico City, Monterrey - New',
        107 => '(GMT -05:00) Bogota, Lima, Quito, Rio Branco',
        332 => '(GMT -05:00) Eastern Time (US & Canada)',
        335 => '(GMT -05:00) Indiana (East)',
        70 => '(GMT -04:00) Atlantic Time (Canda)',
        357 => '(GMT -04:00) Caracas, La Paz',
        61 => '(GMT -04:00) Manaus',
        99 => '(GMT -04:00) Santiago',
        69 => '(GMT -03:30) Newfoundland',
        52 => '(GMT -03:00) Brazilia',
        19 => '(GMT -03:00) Buenos Aires, Georgtown',
        48 => '(GMT -02:00) Mid-Atlantic',
        267 => '(GMT -01:00) Azores',
        175 => '(GMT) Casablanka, Monrovia, Reykjavik',
        140 => '(GMT) Greenwhich Mean Time : Dublin, Edinburgh, Lisbon, London',
        244 => '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna',
        366 => '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague',
        42 => '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris',
        260 => '(GMT+01:00) Sarajeva,Skopje, Warsaw, Zagreb',
        205 => '(GMT+01:00) West Central Africa',
        178 => '(GMT+02:00) Amman',
        154 => '(GMT+02:00) Athens, Bucharest, Istanbul',
        196 => '(GMT+02:00) Beirut',
        123 => '(GMT+02:00) Cario',
        369 => '(GMT+02:00) Harare, Pretoria',
        130 => '(GMT+02:00) Helsinki, Kyiv, Rig, Sofia, Tallin, Vilnius',
        170 => '(GMT+02:00) Jerusalem',
        67 => '(GMT+02:00) Minsk',
        238 => '(GMT+02:00) Windhoek',
        173 => '(GMT+03:00) Baghdad',
        190 => '(GMT+03:00) Kuwait, Riyadh',
        274 => '(GMT+03:00) Moscow, St. Petersburg, Volgograd',
        180 => '(GMT+03:00) Nairobi',
        143 => '(GMT+03:00) Tbilisi',
        174 => '(GMT+03:00) Tehran',
        251 => '(GMT+04:00) Abu Dhabi, Muscat',
        38 => '(GMT+04:00) Baku',
        7 => '(GMT+04:00) Yerevan',
        3 => '(GMT+04:00) Kabul',
        276 => '(GMT+05:00) Ekaterinburg',
        354 => '(GMT+05:00) Islamabad, Karachi, Tashkent',
        192 => '(GMT+06:00) Almaty, Novosibirsk',
        311 => '(GMT+07:00) Bangkok, Hanoi, Jakarta',
        279 => '(GMT+07:00) Krasnoyarsk',
        280 => '(GMT+08:00) Irkutsk, Ulaan Bataar',
        36 => '(GMT+08:00) Perth',
        321 => '(GMT+08:00) Taipei',
        189 => '(GMT+09:00) Seoul',
        281 => '(GMT+09:00) Yakutsk',
        34 => '(GMT+09:30) Adelaide',
        35 => '(GMT+09:30) Darwin',
        32 => '(GMT+10:00) Brisbane',
        157 => '(GMT+10:00) Guam, Port Moresby',
        28 => '(GMT+10:00) Hobart',
        282 => '(GMT+10:00) Vladivostok',
        283 => '(GMT+11:00) Magadan, Solomon Is., New Caledonia',
        249 => '(GMT+12:00) Auckland, wellington',
        131 => '(GMT+12:00) Fiji, Kamchatka, Marshall Is.'
);

require_once('html.php');
require_once('xmlrpc.inc');



function get_account_by_login($login) {
	global $packages;
	$params = array(new xmlrpcval(array("username"      => new xmlrpcval($login, "string")),'struct'));
	$cli = new xmlrpc_client('https://login.fasttalks.com/xmlapi/xmlapi');
	$cli->setSSLVerifyPeer(false);
        $cli->setCredentials(XML_LOGIN, XML_PASWORD, CURLAUTH_DIGEST);

        $msg = new xmlrpcmsg('getAccountInfo', $params);
	
        $r = $cli->send($msg, 20);       /* 20 seconds timeout */
	
 	if ($r->faultCode()) {
                /* $re "Fault. Code: " . $r->faultCode() . ", Reason: " . $r->faultString(); */
		$xml_error = $r->faultString();
                return array();
        }

        $success_fetch = $r->value();

	$tmp = $success_fetch->structMem('i_account');
        $i_account = $tmp->scalarVal();
	
	$tmp = $success_fetch->structMem('email');
        $email = $tmp->scalarVal();
	
	$tmp = $success_fetch->structMem('username');
        $username = $tmp->scalarVal();


	return array ('i_account' => $i_account, 'email' => $email, 'username' => $username);
}	

function reset_password($username) {
        global $packages;
        $params = array(new xmlrpcval(array("username"      => new xmlrpcval($username, "string")),'struct'));
        $cli = new xmlrpc_client('https://login.fasttalks.com/xmlapi/xmlapi');
        $cli->setSSLVerifyPeer(false);
        $cli->setCredentials(XML_LOGIN, XML_PASWORD, CURLAUTH_DIGEST);

        $msg = new xmlrpcmsg('resetAccountOneTimePassword', $params);

        $r = $cli->send($msg, 20);       /* 20 seconds timeout */

        if ($r->faultCode()) {
                /* $re "Fault. Code: " . $r->faultCode() . ", Reason: " . $r->faultString(); */
                $xml_error = $r->faultString();
                return array();
        }

        $success_fetch = $r->value();

        $tmp  = $success_fetch->structMem('password');
        $password = $tmp->scalarVal();
	

        return  array ('password' => $password);
}



function login_xml($login, $password) {
	global $packages;

	$params = array(new xmlrpcval(array("username"      => new xmlrpcval($login, "string"),
         		"password"      => new xmlrpcval($password, "string")),'struct'));
    	$cli = new xmlrpc_client('https://login.fasttalks.com/xmlapi/xmlapi');
	$cli->setSSLVerifyPeer(false);
	$cli->setCredentials(XML_LOGIN, XML_PASWORD, CURLAUTH_DIGEST);
	
	$msg = new xmlrpcmsg('authAccount', $params);

	$r = $cli->send($msg, 20);       /* 20 seconds timeout */

	if ($r->faultCode()) {
      		/* $re "Fault. Code: " . $r->faultCode() . ", Reason: " . $r->faultString(); */
 		$xml_error = $r->faultString();
		return array(0,$xml_error);
	}
	
	$success_login = $r->value();
        
	$tmp = $success_login->structMem('i_account');
        $i_account = $tmp->scalarVal();

	$params = array(new xmlrpcval(array("i_account"  => new xmlrpcval("$i_account", "int"),
		                ), 'struct'));
	$msg = new xmlrpcmsg('getAccountInfo', $params);
	$cli = new xmlrpc_client('https://login.fasttalks.com/xmlapi/xmlapi');
	$cli->setSSLVerifyPeer(false);
	$cli->setCredentials(XML_LOGIN, XML_PASWORD, CURLAUTH_DIGEST);
	$r = $cli->send($msg, 20);       /* 20 seconds timeout */

	if ($r->faultCode()) {
		$xml_error = $r->faultString();
                return array(0,$xml_error);
	}

	$success_fetch = $r->value();
	
	$i_billing_plan_s  = $success_fetch->structMem('i_billing_plan');
	$i_billing_plan = $i_billing_plan_s->scalarVal();

	$authname_s  = $success_fetch->structMem('authname');
	$authname = $authname_s->scalarVal();

	$balance_s  = $success_fetch->structMem('balance');
       	$balance = $balance_s->scalarVal();

	/* find package*/
	foreach($packages as $key => $package) {
		if($package['i_billing_plan'] == $i_billing_plan) $i_package = $key;
	}

    	return array(1,$i_account,$i_package, $authname, $balance);
}


function updateAccount() {
	$params = array(new xmlrpcval(array(	"i_account"     => new xmlrpcval($_SESSION['i_account'], "int"),
						"first_name" 	=> new xmlrpcval(get_par('first_name'), "string"),
						"last_name"    	=> new xmlrpcval(get_par('last_name'), "string"),
						"email"    	=> new xmlrpcval(get_par('email'), "string"),
						"i_time_zone"   => new xmlrpcval(get_par('i_time_zone'), "int"),
						"company_name"  => new xmlrpcval(get_par('company_name'), "string"),
						"street_addr"   => new xmlrpcval(get_par('street_addr'), "string"),
						"phone"    	=> new xmlrpcval(get_par('phone'), "string"),), 'struct'));
	$msg = new xmlrpcmsg('updateAccount', $params);
	$cli = new xmlrpc_client('https://login.fasttalks.com/xmlapi/xmlapi');
        $cli->setSSLVerifyPeer(false);
	$cli->setCredentials(XML_LOGIN, XML_PASWORD, CURLAUTH_DIGEST);

	$r = $cli->send($msg, 20);

	if ($r->faultCode()) {
		/* $re "Fault. Code: " . $r->faultCode() . ", Reason: " . $r->faultString(); */
		$error = $r->faultString();
		return array (false, $error);
	}
	return array (true);
}

function updateAccountPassword($i_account,$password) {
	$params = array(new xmlrpcval(array(    "i_account"     => new xmlrpcval($i_account, "int"),
						"web_password"	=> new xmlrpcval($password, "string"),), 'struct'));
	$msg = new xmlrpcmsg('updateAccount', $params);
        $cli = new xmlrpc_client('https://login.fasttalks.com/xmlapi/xmlapi');
        $cli->setSSLVerifyPeer(false);
        $cli->setCredentials(XML_LOGIN, XML_PASWORD, CURLAUTH_DIGEST);

        $r = $cli->send($msg, 20);

        if ($r->faultCode()) {
                /* $re "Fault. Code: " . $r->faultCode() . ", Reason: " . $r->faultString(); */
                $error = $r->faultString();
                return array (false, $error);
        }
        return array (true);
}


function getPaymentsList($limit, $offset) {
	$params = array(new xmlrpcval(array(    "i_account"     => new xmlrpcval($_SESSION['i_account'], "int"),
                                                "limit"         => new xmlrpcval($limit, "int"),
                                                "offset"         => new xmlrpcval($offset, "int"),), 'struct'));
	$msg = new xmlrpcmsg('getPaymentsList', $params);
        $cli = new xmlrpc_client('https://login.fasttalks.com/xmlapi/xmlapi');
        $cli->setSSLVerifyPeer(false);
        $cli->setCredentials(XML_LOGIN, XML_PASWORD, CURLAUTH_DIGEST);

        $r = $cli->send($msg, 20);       /* 20 seconds timeout */

        if ($r->faultCode()) {
                $error = $r->faultString();
                return array();
        }

	$res = $r->value()->structMem('payments');
	$res = $res->scalarVal();
        $payments = array();
        $i=0;


	 foreach( $res as $obj ) {
                $payment_time = $obj->structMem('payment_time');
                $payment_time = $payment_time->scalarVal();

                $amount = $obj->structMem('amount');
                $amount = $amount->scalarVal();

                $currency = $obj->structMem('currency');
                $currency = $currency->scalarVal();

                $notes = $obj->structMem('notes');
                $notes = $notes->scalarVal();

                $payments[$i]['payment_time'] = $payment_time ;
                $payments[$i]['amount'] = sprintf("%01.2f",$amount); 
                $payments[$i]['currency'] = $currency;
                $payments[$i]['notes'] = $notes;
                $i++;
        }
        return $payments;

}


function getAccountCDRs($limit, $offset) {
	$params = array(new xmlrpcval(array(	"i_account"     => new xmlrpcval($_SESSION['i_account'], "int"),
						"limit" 	=> new xmlrpcval($limit, "int"),
						"offset"         => new xmlrpcval($offset, "int"),), 'struct'));
	$msg = new xmlrpcmsg('getAccountCDRs', $params);
	$cli = new xmlrpc_client('https://login.fasttalks.com/xmlapi/xmlapi');
        $cli->setSSLVerifyPeer(false);
        $cli->setCredentials(XML_LOGIN, XML_PASWORD, CURLAUTH_DIGEST);

	$r = $cli->send($msg, 20);       /* 20 seconds timeout */

	if ($r->faultCode()) {
		$error = $r->faultString();
		return array();
	}
	
	$res = $r->value()->structMem('cdrs');
	$res = $res->scalarVal();
	$cdrs= array();
	$i=0;

	foreach( $res as $obj ) {
		$cli = $obj->structMem('cli');
		$cli = $cli->scalarVal();

		$billed_duration = $obj->structMem('billed_duration');
		$billed_duration = $billed_duration->scalarVal();

		$cld = $obj->structMem('cld');
		$cld = $cld->scalarVal();

		$country = $obj->structMem('country');
		$country = $country->scalarVal();

		$connect_time = $obj->structMem('connect_time');
		$connect_time = $connect_time->scalarVal();

		$cost = $obj->structMem('cost');
		$cost = $cost->scalarVal();
		
		$duration = sprintf("%02d:%02d",ceil($billed_duration/60)-1, fmod($billed_duration,60));

		$cdrs[$i]['cli'] = $cli ;
		$cdrs[$i]['duration'] = $duration;
		$cdrs[$i]['cld'] = $cld;
		$cdrs[$i]['country'] = $country;
		$cdrs[$i]['connect_time'] = $connect_time;
		$cdrs[$i]['cost'] = sprintf("%01.2f USD",$cost);
		$i++;
	}

	return $cdrs;

}

function getAccount() {

        $params = array(new xmlrpcval(array("i_account"      => new xmlrpcval($_SESSION['i_account'], "int")), 'struct'));
        $msg = new xmlrpcmsg('getAccountInfo', $params);

        $cli = new xmlrpc_client('https://login.fasttalks.com/xmlapi/xmlapi');
        $cli->setSSLVerifyPeer(false);
        $cli->setCredentials(XML_LOGIN, XML_PASWORD, CURLAUTH_DIGEST);

        $r = $cli->send($msg, 20);       /* 20 seconds timeout */

        if ($r->faultCode()) {
                /* $re "Fault. Code: " . $r->faultCode() . ", Reason: " . $r->faultString(); */
                $error = $r->faultString();
                return false;
        }

        $success_fetch = $r->value();

        $first_name_s  = $success_fetch->structMem('first_name');
        $first_name = $first_name_s->scalarVal();

        $last_name_s  = $success_fetch->structMem('last_name');
        $last_name = $last_name_s->scalarVal();

        $email_s  = $success_fetch->structMem('email');
        $email = $email_s->scalarVal();

        $i_time_zone_s  = $success_fetch->structMem('i_time_zone');
        $i_time_zone = $i_time_zone_s->scalarVal();

        $company_name_s  = $success_fetch->structMem('company_name');
        $company_name = $company_name_s->scalarVal();

        $street_addr_s  = $success_fetch->structMem('street_addr');
        $street_addr = $street_addr_s->scalarVal();

        $phone_s  = $success_fetch->structMem('phone');
        $phone = $phone_s->scalarVal();

        $params = array(new xmlrpcval(array("i_account"      => new xmlrpcval($_SESSION['i_account'], "int")), 'struct'));
        $msg = new xmlrpcmsg('getAccountMinutePlans', $params);

        $ret = array(   'first_name' => $first_name,
                        'last_name' => $last_name,
                        'email' => $email,
                        'i_time_zone' => $i_time_zone,
                        'company_name' => $company_name,
                        'street_addr' => $street_addr,
                        'phone' => $phone
                );
        $cli = new xmlrpc_client('https://login.fasttalks.com/xmlapi/xmlapi');
        $cli->setSSLVerifyPeer(false);
        $cli->setCredentials(XML_LOGIN, XML_PASWORD, CURLAUTH_DIGEST);

        $r = $cli->send($msg, 20);       /* 20 seconds timeout */

        if ($r->faultCode()) {
                /* $re "Fault. Code: " . $r->faultCode() . ", Reason: " . $r->faultString(); */
                $error = $r->faultString();
                return false;
        }

        $success_fetch = $r->value();

        $minute_plans_s  = $success_fetch->structMem('minute_plans');
        $minute_plans = $minute_plans_s->scalarVal();

        if(count($minute_plans)) {
                $minute_plans = $minute_plans[0];

                $seconds_total_s = $minute_plans->structMem('seconds_total');
                $seconds_total = $seconds_total_s->scalarVal();

                $seconds_left_s = $minute_plans->structMem('seconds_left');
                $seconds_left = $seconds_left_s->scalarVal();

                $chargeable_seconds_s = $minute_plans->structMem('chargeable_seconds');
                $chargeable_seconds = $chargeable_seconds_s->scalarVal();

                $ret['seconds_total'] = $seconds_total;
                $ret['seconds_left'] = $seconds_left;
                $ret['chargeable_seconds'] = $chargeable_seconds;

        }

        return $ret;
}

?>

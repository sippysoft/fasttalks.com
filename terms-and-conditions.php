<? 
$title = "Terms And Conditions";

require_once('header.php'); ?>
	<div id="middle">

		<div id="container">
			<div id="contentHowItWorks">
				<div class="howItWorks_txt blue font_26">
                	Terms And Conditions
       	       </div><!-- howItWorks_txt-->
                <div class="steps text_justify">

<p>
				These Terms and Conditions, together with the Privacy Policy, govern the use of this website and all services provided by FastTalks ("FastTalks," "We," "Us"), and should be read carefully. 
By using the website located at www.fasttalks.com and its subdirectories (the "Website"), or any service provided via the Website (the "Service") the user ("You") agree to, and accept these terms and conditions in full. If You disagree with any part of this disclaimer, do not use the Website, as that condition prohibits You from doing so. These terms apply to You whether or not you click "Sign up" through any registration process on the Website.
FastTalks reserves the right to modify these terms at any time. If there are any changes to the terms or Privacy Policy, FastTalks will announce these changes have been made on the Website's homepage. Any changes will be posted on the Website 30 days prior to these changes taking place. You are therefore advised to re-read this statement on a regular basis.
 By using the Website after changes have been made to this document, You agree to accept those changes, whether or not You have reviewed them.
</p>
<p>
<b>Payment</b>
Unless stated otherwise, all rates and charges shall be stated in U.S. dollars and shall be exclusive of any applicable taxes.
Your payments for FastTalks' Call Us service are made by charging Your credit card on a subscription basis. Your credit card will be charged automatically every month according to the subscription fee for the chosen plan. Charges are made in advance for the succeeding month.
If You use up all the call minutes included in Your monthly plan, the Service will cease to function until You purchase additional minutes. Call minutes included as part of your plan expire at the end of the month.
If FastTalks is unable to charge Your credit card, You will be sent an email with instructions to modify Your credit card details. In this case, the Service will be suspended until the credit card is successfully charged. You will not be charged for the period Your account was suspended.
FasTalks reserves the right to change the rates and plans at any time without notice. If You do not wish to accept such changes, You are entitled to terminate Your account with effect from the date on which the adjustment becomes effective. You agree that by continuing to use the Service following any adjustment of rates or plans, You accept such adjustments.
If You believe FastTalks has charged you in error, You must contact FastTalks customer services at support@fasttalks.com within 60 days. No refunds will be given for any charges more than 60 days old.
</p>
<p>


<b>Acceptable Use</b>
When creating an account or using the Service, You shall: 
Abide by all applicable laws and regulations relating to the capture, use, transmission and distribution of voice, content, software or other matter; conform to applicable laws and regulations pertaining to the use of telephone networks, telemarketing, automated voice broadcasts, automated dialers, call or voice recording or surveillance, and electronic mail; and provide us with accurate user information including telephone numbers and contact information.
You or another account user shall not, nor allow any person to: Use the Service in any manner that violates the rights of any person or is illegal, fraudulent, deceptive, harassing, threatening, harmful, libelous, defamatory, abusive, slanderous, hateful, sexually, racially or ethnically objectionable, vulgar, pornographic, obscene, invasive of privacy, or otherwise objectionable or unlawful; attempt to gain unauthorized access to any account, the FastTalks network, or any related components; or interfere with another person's use and enjoyment of the Service.
</p>
<p>

<b>Termination of Service</b>
The agreement to use the Service is effective as of the date on which this agreement is entered into by clicking on the "Sign up" button, and will remain effective until terminated by either You or FastTalks. 
FastTalks reserves the right to refuse, cancel, or suspend the Service at our sole discretion, including services that are already underway. No refunds shall be offered where the Service is deemed to have begun. Any money that has been paid to Us that constitute payment in respect of the provision of unused services shall be refunded.
You may terminate this agreement with immediate effect at any time, with or without cause, provided that you will cease any and all use of the Service and you will remove the Service from any of your systems in your possession or under your control. To terminate an account, send a request to support@fasttalks.com.
</p>
<p>

<b>Website Links</b>
FastTalks is not responsible for the contents or reliability of any other websites to which we provide a link from the Website, and we do not, expressly or otherwise, endorse the views and/or content expressed within those sites.
FastTalks is not responsible for the privacy practices, or content, of these sites. FastTalks encourages You to be aware when leaving the Website to read the privacy statements of these sites. You should evaluate the security and trustworthiness of any other site connected to the Website or accessed through the Website yourself, before disclosing any personal information to them. 
FastTalks will not accept any responsibility for any loss or damage in whatever manner, howsoever caused, resulting from your disclosure to third parties of personal information.
</p>
<p>

<b>Limitation of Liability</b>
FastTalks does not warrant that the Service will be uninterrupted, timely or error free, although it is provided to the best ability. If the Service becomes unavailable, we will inform You as soon as possible when it will be expected to return to normal.
We may at any time change, modify, suspend, or discontinue any aspect of the Service, including, without limitation, the availability of any Service feature, database or content, hours of availability, or equipment needed to access the Service. We may also impose limits on certain features or restrict your access to parts or all of the Service without notice or liability.
FastTalks may change technical features in order to keep pace with the latest demands and developments or to comply with regulations. FastTalks may also have to repair and/or upgrade the Service and this may require Us to restrict, limit, and/or interrupt the Service.
FastTalks shall not assume or have any liability for any action by FastTalks or its affiliates and licensees with respect to the use of content. 
To the fullest extent permitted by applicable law, including but not limited to negligence, FastTalks and its officers, employees, contractors or content providers shall not be liable for any indirect, incidental, special or consequential damages (including, without limitation, damages or loss of business, lost profits, business interruption, loss of business information, or any other pecuniary loss, even if FastTalks has been advised of the possibility of such damages, in connection with the Service, or resulting from the use of or the inability to use the Service or any transaction entered into through or from FastTalks, or from unauthorized access to or alteration of your transmissions, data, or account. 
</p>
<p>

<b>Force Majeure</b>
Including You and FastTalks, no party related to any aspect of the Service shall be liable to the other for any failure to perform any obligation under any agreement which is due to an event beyond the control of such party including but not limited to any Act of God, terrorism, war, Political insurgence, insurrection, riot, civil unrest, act of civil or military authority, uprising, earthquake, flood or any other natural or man-made eventuality outside of our control, which causes the termination of an agreement or contract entered into, nor which could have been reasonably foreseen. Any party affected by such event shall forthwith inform the other party of the same and shall use all reasonable endeavours to comply with the terms and conditions of any agreement contained herein.
</p>
<p>

<b>Disclaimer</b>
The Service is provided on an "as is" basis. To the fullest extent permitted by law, FastTalks excludes all representations and warranties relating to the Service which is or may be provided by any affiliates or any other third party, including in relation to any inaccuracies or omissions in the Website. 
FastTalks also excludes all liability for damages arising out of or in connection with Your use of the Service, to the extent permitted by law, as outlined above. 
Some jurisdictions do not allow exclusions of an implied warranty, so portions of this disclaimer may not apply to You and You may have other legal rights according to Your jurisdiction.
</p>
<p>

<b>Governing Law</b>
The Federal laws of Canada and the Provincial laws of British Columbia shall otherwise govern use of the Service where these terms and conditions are not clear or incomplete and you hereby agree to submit to the exclusive jurisdiction of the Canadian Federal and Provincial court system.
For all disputes, with regard to the Service or any term listed in this policy, the jurisdiction that shall be used in determining liability is the court situated in British Columbia, Canada. If FastTalks is obligated to go to court, rather than arbitration, to enforce any of its rights, or to collect any fees, You agree to reimburse FastTalks for its legal fees, costs and disbursements if FastTalks is successful. You agree that the Courts of British Columbia, Canada are the agreed and appropriate forums for any such suit, and consent to service of process by registered mail or overnight courier with proof of delivery.
</p>
<p>

<b>Severability</b>
If any term or provision of this agreement is found to be unenforceable for any reason, this agreement shall remain in full force and effect and shall be fully enforceable on its remaining terms and conditions. 
</p>
<p>

<b>Copyright</b>
All materials on this site are protected by copyright and intellectual property laws and are the property of FastTalks.
All products pertaining to the Service are property of FastTalks. You may not offer any of our products, modified or unmodified, for redistribution or resale of any kind without prior written consent from FastTalks. You may not claim intellectual or exclusive ownership to any of our products, modified or unmodified. 
Your rights to the Service are limited to a non-exclusive and non-transferable runtime right solely for the purpose of using the Service. 
No part of the Service may be copied, reproduced, republished, reused, uploaded, downloaded, posted, or transmitted, other than through the Service in accordance with its intended use, nor may derivative works be created from it or distributed in any way.
(c) FastTalks 2011 All Rights Reserved
</p>
<p>


                </div><!-- steps-->                            	
            <div class="clearfix"></div>    
		  </div><!-- #content-->
		</div><!-- #container-->       

	</div><!-- #middle-->

<? require_once('footer.php'); ?>

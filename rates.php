<? require_once('header_logged_in.php'); ?>
	<div id="middle">


	  <div id="contentClient">
         <div class="allRates_wrap">         	
         	<div class="allRates ">
            	<div class="index_wrap">
            	<span class="font_26 blue">ALL rates</span>
                <div class="letter bold font_26" >
                	A
                </div><!-- letter-->
                </div><!-- index_wrap-->
  <ul>
                	<li><a href="#">A</a></li>
                    <li><a href="#">B</a></li>
                    <li><a href="#">C</a></li>
                    <li><a href="#">D</a></li>
                    <li><a href="#">E</a></li>
                    <li><a href="#">F</a></li>
                    <li><a href="#">G</a></li>
                    <li><a href="#">H</a></li>
                    <li><a href="#">I</a></li>
                    <li><a href="#">J</a></li>
                    <li><a href="#">K</a></li>
                    <li><a href="#">L</a></li>
                    <li><a href="#">M</a></li>
                    <li><a href="#">N</a></li>
                    <li><a href="#">O</a></li>
                    <li><a href="#">P</a></li>
                    <li><a href="#">Q</a></li>
                    <li><a href="#">R</a></li>
                    <li><a href="#">S</a></li>
                    <li><a href="#">T</a></li>
                    <li><a href="#">U</a></li>
                    <li><a href="#">V</a></li>
                    <li><a href="#">W</a></li>
                    <li><a href="#">Y</a></li>
                    <li><a href="#">Z</a></li>
                </ul>
            </div><!-- allRates-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr class="td_color_head white font_20">
                <td width="457" >Destination:</td>
                <td width="457">Price:</td>
              </tr>
              <tr class="td_color_dark font_18">
                <td>Albania Mobile</td>
                <td>$0.44</td>
              </tr>
              <tr class="td_color_lite font_18">
                <td>Albania (Zone 1) </td>
                <td>$0.09</td>
              </tr>
              <tr class="td_color_dark font_18">
                <td>Albania (Zone 2) </td>
                <td>$1.7</td>
              </tr>
              <tr class="td_color_lite font_18">
                <td>Algeria Mobile (Zone 1)</td>
                <td> $0.24</td>
              </tr>
              <tr class="td_color_dark font_18">
                <td>Algeria Mobile (Zone 2) </td>
                <td>$0.44</td>
              </tr>
              <tr class="td_color_lite font_18">
                <td>Algeria (Zone 1) </td>
                <td>$0.11</td>
              </tr>
            </table>

         </div><!-- allRates_wrap-->
	  </div><!-- contentClient-->
	</div><!-- #container-->
		
  </div><!-- #middle-->

<? require_once('footer.php'); ?>
<? 
require_once('packages.php');

session_start();

if($_SESSION['i_account'] > 0) {
	header('Location: information.php');
}

$translate_errors = array(
	"Unauthorized" => "Your login attempt has failed. The username or password are incorrect"
	);

if (!empty($_POST["username"])) {
	$login_result = login_xml($_POST["username"], $_POST["password"]);
	$success_login = $login_result[0];
	if($success_login) {
		$i_account = $login_result[1];
		$i_package = $login_result[2];
		$authname = $login_result[3];
		$balance = (float)$login_result[4];
		$balance = abs($balance);

		$_SESSION['i_account'] = $i_account;
		$_SESSION['i_package'] = $i_package;
		$_SESSION['authname'] = $authname;
		$_SESSION['balance'] = $balance;

		header('Location: information.php');
	} else {
		$error_message = strtr($login_result[1], $translate_errors);
	}

}
$title = "Log In";

require_once('header.php');

?>

	<div id="middle">

		<div id="container">
			<div id="content">
              <div class="loginSection_wrap">	
            	<div class="loginSection_left">
                   <div class="loginSection_right">
	<!--		<form action="https://login.fasttalks.com/main.php" method="post">  -->
			<form action="login.php" method="post">

			 	<input type="hidden" name="acct_type" value="account"> 
			  	<input type="hidden" name="return_to_referer" value=1> 
		  		<input type="hidden" name="login_page" value=account> 
		  		<input type="hidden" name="Login" value="Login"> 


						<div class="loginSection">
						<center><? echo $error_message; ?> </center>
    						<table width="573" bode="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="125" height="45">User Name</td>
                                <td colspan="3" style="padding:5px 0 0 10px;"  >                                
                                <div class= "left-input"><div class= "right-input"><div class= "fill-input">
	                                <input type="text" name="username" />
                                </div></div></div>                                 
                                </td>
                              </tr>
                              <tr>
                                <td height="45">Password</td>
                                <td colspan="3" style="padding:5px 0 0 10px;">
                                <div class= "left-input"><div class= "right-input"><div class= "fill-input">
	                                <input type="password" name="password"/>
                                </div></div></div>                                 

                                </td>
                              </tr>
                              <tr>
                                <td height="30">&nbsp;</td>
                                <td width="34" align="center"><input type="checkbox" /></td>
                                <td width="216">Remember me</td>
                                <td width="188" rowspan="2" align="center"><input type="image" src="img/login_button.png" OnClick="this.form.submit();"/></td>
			</form>
                              </tr>
                              <tr>
                                <td height="45">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><a href="forgot-your-password.php" class="blue">Forgot you password?</a></td>
                              </tr>
                                                            <tr>
                                <td height="38">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>Don't have an account?  </td>
                                <td align="center"><a href="sign-up.php" class="blue">Sign up for free</a></td>
                              </tr>
                            </table>

</div><!-- loginSection-->
	                </div><!-- loginSection_ight-->
                </div><!-- loginSection_left-->
               </div><!-- loginSection_wap--> 
            </div><!-- #content-->
		</div><!-- #containe-->
		
        <div class="sidebar" id="sideRight">

                    


		</div><!-- .sidebar#sideRight -->		

	</div><!-- #middle-->
<? require_once('footer.php'); ?>

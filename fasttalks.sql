-- MySQL dump 10.11
--
-- Host: localhost    Database: fasttalks
-- ------------------------------------------------------
-- Server version	5.0.91

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `buttons`
--

DROP TABLE IF EXISTS `buttons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buttons` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  `i_account` int(11) NOT NULL,
  `numbers` varchar(120) default NULL,
  `call_label` varchar(100) default NULL,
  `cancel_label` varchar(100) default NULL,
  `disconnect_label` varchar(100) default NULL,
  `progress_label` varchar(100) default NULL,
  `preset` varchar(7) default NULL,
  `widget_width` int(11) NOT NULL,
  `widget_height` int(11) NOT NULL,
  `button_width` int(11) NOT NULL,
  `button_height` int(11) NOT NULL,
  `active` int(11) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buttons`
--

LOCK TABLES `buttons` WRITE;
/*!40000 ALTER TABLE `buttons` DISABLE KEYS */;
INSERT INTO `buttons` VALUES (9,'New button',20,'380913104703','Call Us','Cancel','Disconnect','Estabilishing connection...','5555FF',215,138,160,150,1),(10,'Main page button',21,'17786284020','Call Us','Cancel','Disconnect','Estabilishing connection...','ff7603',240,100,160,40,1),(5,'New button',16,'','Call Us','Cancel','Disconnect','Estabilishing connection...','ff7603',215,138,160,150,1),(6,'My widget',18,'17786284020','Call me','Cancel','Disconnect','Estabilishing connection...','000000',215,70,160,70,1),(19,'New button2',15,'380913104703,18667478647','Call Uszzzz','Cancelzz','Disconnectzz','Estabilishing connection...zz','ff7603',240,100,140,30,1);
/*!40000 ALTER TABLE `buttons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passreset`
--

DROP TABLE IF EXISTS `passreset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passreset` (
  `id` int(11) NOT NULL auto_increment,
  `i_account` int(11) NOT NULL,
  `username` varchar(100) default NULL,
  `secret_string` varchar(100) default NULL,
  `used` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passreset`
--

LOCK TABLES `passreset` WRITE;
/*!40000 ALTER TABLE `passreset` DISABLE KEYS */;
INSERT INTO `passreset` VALUES (1,15,'test107','55639fa073716d6498b999799362d5f5',1);
/*!40000 ALTER TABLE `passreset` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-08-12  5:49:48

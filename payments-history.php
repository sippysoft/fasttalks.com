<?
require_once('mysql.php');
require_once('html.php');
require_once('xmlrpc.inc');
require_once('packages.php');


session_start();

$payments_per_page = 5;
$page = (int) get_par('page');
$payments = getPaymentsList($payments_per_page, $page*$cdrs_per_page);
$payments_count = count($payments);

$title = "Payments History";

require_once('header_logged_in.php'); ?>    
	<div id="middle">


	  <div id="contentClient">
            <div class="profile_edit_txt">
            	<span class="font_26 blue">Payments History</span>  
            </div><!-- profile_edit_txt-->	
	<div class="paymentsHistory">  
            	<table width="550" border="0" bgcolor="#dbeefc" cellpadding="5">
                  <tr align="center" class="bg_head_payments white font_18">
                    <td width="250">Date </td>
                    
                    <td width="50">Amount</td>
                    
                    <td width="246">Comments </td>
		  </tr>
<?
if($payments_count>0) {

        foreach($payments as $payment) {
                echo  "
<tr class=\"border_bottom_payments\">
        <td>".$payment['payment_time']."</td>
        <td>".$payment['amount']."</td>
        <td>".$payment['notes']."</td>
</tr>
";

        }
} else {
        echo "<td colspan=5>No payments found</td>";
}

?>



                </table>

	
    <br />
                <div class="pagination">
                        <ul>
<?
if($page>0) {
        echo "<li><a href=payments-history.php?page=".($page-1).">&#60;newer</a></li>";
} else echo "<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>";

        echo "<li><a>|</a></li>";
if($payments_count==$payments_per_page) {
        echo "<li><a href=payments-history.php?page=".($page+1).">older&#62;</a></li>";
} else echo "<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>";
?>
                    </ul>
                </div><!-- pagination-->

                </div><!-- profileContent-->               
                
                
	  </div><!-- contentClient-->
		</div><!-- #container-->
		
  </div><!-- #middle-->

<? require_once('footer.php'); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><? echo $title; ?></title>
	<meta name="title" content="" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    <!--[if lt IE 7]>
	<script defer type="text/javascript" src="script/pngfix.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="screen, projection" />
</head>

<body>

<div id="wrapper">

	<div id="header">
    	<div class="logo">
        	<a href="index.php"><img src="img/logo.jpg" title="FastTalks.com" width="178" height="104" alt="FastTalks.com" /></a>
        </div><!-- .logo-->
        <div class="login">
        	<ul>
            	<li><a href="sign-up.php"><img src="img/singUp.png" width="112" height="37" alt="Sign up" /></a></li>	
                <li><a href="login.php"><img src="img/login.png" alt="" width="112" height="37" /></a></li>
            </ul>
        </div><!-- login-->
  <div class="menu">
        	<ul>
		<li <? if($_SERVER["SCRIPT_NAME"]=='/index.php') echo 'class="active"'; ?>><a  href="index.php">Home</a></li>
		<li <? if($_SERVER["SCRIPT_NAME"]=='/pricing.php') echo 'class="active"'; ?> ><a href="pricing.php">Pricing</a></li>
		<li <? if($_SERVER["SCRIPT_NAME"]=='/how-it-works.php') echo 'class="active"'; ?>><a href="how-it-works.php">How It Works</a></li>
		<li <? if($_SERVER["SCRIPT_NAME"]=='/about-us.php') {echo 'class="active last"';} else echo 'class="last"'; ?>><a href="about-us.php">About Us</a></li>                
            </ul>
        </div><!-- .menu-->        
      <div class="clearfix"></div>
        
	</div><!-- #header-->

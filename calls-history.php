<? 
require_once('mysql.php');
require_once('html.php');
require_once('xmlrpc.inc');
require_once('packages.php');

session_start();

$cdrs_per_page = 5;
$page = (int) get_par('page');
$cdrs = getAccountCDRs($cdrs_per_page,$page*$cdrs_per_page);
$cdr_count = count($cdrs);

$title = "Call History";

require_once('header_logged_in.php'); ?>

	<div id="middle">


	  <div id="contentClient">
            <div class="profile_edit_txt">
            	<span class="font_26 blue">Calls history</span>  
            </div><!-- profile_edit_txt-->	
	<div class="paymentsHistory">  
            	<table width="550" border="0" bgcolor="#dbeefc" cellpadding="5">
		  <tr align="center" class="bg_head_payments white font_18">
		    <td width="160">CLD </td>
		    <td width="160">CLI </td>
		    <td width="100">Duration</td>
                    <td width="200">Date </td>
                    <td width="100">Amount</td>
                  </tr>
<?
if($cdr_count>0) {

	foreach($cdrs as $cdr) {
		echo  "
<tr class=\"border_bottom_payments\">
	<td>".$cdr['cld']."</td>			
	<td>".$cdr['cli']."</td>
	<td>".$cdr['duration']."</td>
	<td>".$cdr['connect_time']."</td>
	<td>".$cdr['cost']."</td>
</tr>
";
		
	}
} else {
	echo "<td colspan=5>No calls found</td>";
}

?>

                </table>

	
				<br />
                <div class="pagination">
			<ul>
<?
if($page>0) {
	echo "<li><a href=calls-history.php?page=".($page-1).">&#60;newer</a></li>";
} else echo "<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>";

	echo "<li><a>|</a></li>";
if($cdr_count==$cdrs_per_page) {
	echo "<li><a href=calls-history.php?page=".($page+1).">older&#62;</a></li>";
} else echo "<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>";
?>
                    </ul>
                </div><!-- pagination-->
                </div><!-- profileContent-->               
                
                
	  </div><!-- contentClient-->
		</div><!-- #container-->
		
  </div><!-- #middle-->
<? require_once('footer.php'); ?>


<? 

require_once('mysql.php');
require_once('html.php');
require_once('xmlrpc.inc');
require_once('packages.php');

session_start();

$account=getAccount();
$title = "Information";
require_once('header_logged_in.php'); ?>
	<div id="middle">
	  <div id="contentClient">
            <div class="profile_edit_txt">
            	<span class="font_26 blue">Profile </span>  <a href="edit-profile.php" class="font_16 orange bold"> edit</a>
            </div><!-- profile_edit_txt-->	
			<div class="information">  
                <table width="500" border="0" cellspacing="0" cellpadding="0">
  <tr class="bg_td">
    <td  >First name: </td>
    <td class="blue"><? echo $account['first_name'];?></td>
  </tr>
  <tr class="bg_td">
    <td>Last name:</td>
    <td class="blue"><? echo $account['last_name'];?></td>
  </tr>
  <tr class="bg_td">
    <td>Email: </td>
    <td><? echo $account['email'];?></td>
  </tr>
  <tr class="bg_td">
    <td>Location:</td>
    <td class="blue"><? echo $tzs[(int)$account['i_time_zone']]; ?></td>
  </tr>
  <tr class="bg_td">
    <td>Company Name:</td>
    <td class="blue"><? echo $account['company_name'];?></td>
  </tr>
  <tr class="bg_td">
    <td>Adress:</td>
    <td class="blue"><? echo $account['street_addr'];?></td>
  </tr>
  <tr class="bg_td">
    <td>Phone Number: </td>
    <td class="blue"><? echo $account['phone'];?></td>
  </tr>
  <tr>
    <td>Activity this month</td>
    <td>&nbsp;</td>
  </tr>
<? if(array_key_exists('seconds_total',$account)) { ?>
  <tr>
    <td>&nbsp;</td>
    <td class="height_32" >Minutes included into the package: <span class="blue"><? echo round(((int) $account['seconds_total'])/60);?></span></td>
  </tr>
  <tr >
    <td>&nbsp;</td>
    <td class="height_32" >Minutes used this month: <span class="blue"><? echo round(((int) $account['chargeable_seconds'])/60);?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="height_32" >Minutes left: <span class="blue"><? echo round(((int) $account['seconds_left'])/60);?></span></td>
  </tr>
<?} else { ?>
<tr>
    <td>&nbsp;</td>
    <td class="height_32" >Package status: <span class="blue">Waiting for payment</span></td>
  </tr>

<? }?>
  <tr>
    <td>&nbsp;</td>
    <td class="height_32" >Balance: <span class="blue"><? echo $_SESSION['balance']; ?></span></td>
  </tr>
<!--
  <tr>
    <td>&nbsp;</td>
    <td class="height_32" >Next billing date: <span class="blue">28-Feb-2011</span></td>
  </tr>
  <tr class="bg_td">
    <td>&nbsp;</td>
    <td class="height_32" >Due Payment: <span class="blue">$29.00</span></td>
  </tr>
-->
</table>
             
             </div><!-- profileContent-->
               	<div class="currentPackage">
                	<div class="currentPackageHead blue">
	                    Current Package
                    </div><!-- currentPackageHead-->
                    <div class="currentPackageMid">
			<ul>
			<? foreach($packages[$_SESSION['i_package']]['feature_list'] as $feature) {  echo "<li>$feature</li>";} ?>			   
                        </ul>
                        <a  href="upgrade-package.php"><img src="img/changePlan.png" width="153" height="35" alt="Change Plan" /></a>
                    </div><!-- currentPackageMid-->
                    <div class="currentPackageBottom">
                    </div><!-- currentPackageBottom-->
                	
                </div><!-- currentPackage-->
                
                
	  </div><!-- contentClient-->
		</div><!-- #container-->
		
  </div><!-- #middle-->
<? require_once('footer.php'); ?>

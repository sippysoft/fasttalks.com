<?php

// Copyright (c) 2003-2005 Maxim Sobolev. All rights reserved.
// Copyright (c) 2006 Sippy Software, Inc. All rights reserved.
//
// Warning: This computer program is protected by copyright law and
// international treaties. Unauthorized reproduction or distribution of this
// program, or any portion of it, may result in severe civil and criminal
// penalties, and will be prosecuted under the maximum extent possible under
// law.
//
// $Id: html.php,v 1.10 2009/10/19 11:38:04 geka Exp $

$cook = $_COOKIE;

$par = array();
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $par = $_GET;
} elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $par = $_POST;
};

$cook = array_map('form_clean_up_recursive', $cook);
$par  = array_map('form_clean_up_recursive', $par);

/*
 *  Clean up cookies & form varible.  
 */

function form_clean_up_recursive($p_var) {

    if (is_array($p_var)) {
	$t_var = array_map('form_clean_up_recursive', $p_var);
    } else {
	$t_var = get_magic_quotes_gpc() ? stripslashes($p_var) : $p_var;
	$t_var = html_entity_decode($t_var, ENT_COMPAT, 'UTF-8');
    }

    return $t_var;
}

/*
 * get cookie from $cook array
 */
function get_cook($key) {

    global $cook;

    return array_key_exists($key, $cook) ? $cook[$key] : '';

};

/*
 * get parameter from $par array
 */
function get_par($key) {

    global $par;

    return array_key_exists($key, $par) ? $par[$key] : '';

};

/*
 * set parameter in $par array
 */
function set_par($key, $val) {

    global $par;

    $par[$key] = $val;

};

/*
 * check if parameter is set
 */
function isset_par($key) {

    global $par;

    return array_key_exists($key, $par);

};

?>

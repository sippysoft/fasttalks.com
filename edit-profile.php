<? 
require_once('mysql.php');
require_once('html.php');
require_once('xmlrpc.inc');
require_once('packages.php');

session_start();

if(get_par('update') == '1') {
	$res = updateAccount();
	if(!$res[0]) { 
		$error = $res[1];
	} else {
		header('Location:information.php');
	}
}

if(get_par('update_pass') == '1') {
	$res = updateAccountPassword($_SESSION['i_account'], get_par('password'));
        if(!$res[0]) $error = $res[1];
}

$account=getAccount();

$title = "Edit Profile";

require_once('header_logged_in.php'); ?>

	<div id="middle">


	  <div id="contentClient">
            <div class="profile_edit_txt">
            	<span class="font_26 blue">Edit Profile </span> 
	    </div><!-- profile_edit_txt-->	
	<form action="edit-profile.php" method=POST>
	<input type=hidden name=update value=1>
<div class="profileContent">  
                <table width="600" border="0" cellspacing="0" cellpadding="0">

<? if(isset($error)) {echo "
<tr>
    <td width=\"170\"><div style=\"color:#ff0000;\">Update error:</div></td>
    <td colspan=\"2\" class=\"blue\">
	<div style=\"color:#ff0000;\">	$error </div>
    </td>
  </tr>

";} ?>

  <tr>
    <td width="170">First name:</td>
    <td colspan="2" class="blue">
    		<div class= "left-input"><div class= "right-input"><div class="fill-input">
		<input name="first_name" type="text" value="<? echo $account['first_name'];?>" />
            </div></div></div>
    </td>
  </tr>
  <tr >
    <td>Last name:</td>
    <td colspan="2" class="blue">
		   <div class= "left-input"><div class= "right-input"><div class= "fill-input">
		   <input value="<? echo $account['last_name'];?>" type="text" name="last_name"  />
            </div></div></div>
    
    </td>
  </tr>
  <tr >
    <td>Email: </td>
    <td colspan="2">
		    <div class= "left-input"><div class= "right-input"><div class= "fill-input">
		    <input value="<? echo $account['email'];?>" type="text" name="email"  />
            </div></div></div>
    
    </td>
  </tr>
  <tr >
    <td>Location:</td>
    <td colspan="2" class="blue">
    <div class="lineForm">
	    <form action="#">	        
                <select class="sel80" id="timeZone" name="i_time_zone">
<?
    foreach ($tzs as $k => $v) {
        $selected = ($account['i_time_zone'] == $k) ? ' selected' : '';
        print "<option value=\"$k\" label=\"$v\"$selected>$v</option>\n";
    }
?>   
		</select>
    	</form>                    
    </div>	          
    </td>
  </tr>
  <tr >
    <td>Company Name:</td>
    <td colspan="2" class="blue">
		   <div class= "left-input"><div class= "right-input"><div class= "fill-input">
	          <input value="<? echo $account['company_name'];?>" type="text" name="company_name"  />
            </div></div></div>
    
    </td>
  </tr>
  <tr >
    <td>Adress:</td>
    <td colspan="2" class="blue">
    		<div class= "left-input"><div class= "right-input"><div class= "fill-input">
	          <input value="<? echo $account['street_addr'];?>" type="text" name="street_addr" />
            </div></div></div>
    </td>
  </tr>
  <tr >
    <td>    
    Phone Number: </td>
    <td colspan="2" class="blue">
    		<div class= "left-input"><div class= "right-input"><div class= "fill-input">
	          <input value="<? echo $account['phone'];?>" type="text" name="phone"  />
            </div></div></div>
    </td>
  </tr>
 <script type="text/javascript">
$().ready(function() {
  $('#dialog_pass').jqm();
});
</script>
</form>
  <tr>
    <td><input type="image" src="img/save_button.png"  /></td>
    <td width="224" align="center"><input type="button" value="" class="cancelButton" onClick="window.location.href='edit-profile.php';" /> </a> </td>
    <td width="198" align="center"><a href="#" class="jqModal"><img src="img/changePass_button.png" width="152" height="35" alt="change password" /></a></td>
  </tr>
</table>
              	
                </div><!-- profileContent-->               
                
<!-- ---------------------------------->
	<div id="dialog_pass" class="jqmWindow password_reset">

  <script type="text/javascript">
    function checkPass(){
      //Store the password field objects into variables ...
      var pass1 = document.getElementById('pass1');
      var pass2 = document.getElementById('pass2');
      //Store the Confimation Message Object ...
      var message = document.getElementById('confirmMessage');
      //Set the colors we will be using ...
      var goodColor = "#66cc66";
      var badColor = "#ff6666";
      //Compare the values in the password field 
      //and the confirmation field
      if(pass1.value == pass2.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
	return true;
      }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
	return false;
      }
    }  
  </script>

<form action="edit-profile.php" method=POST>
<input type=hidden name=update_pass value=1>

                       <table width="640" border="0">
              <tr>
                <td width="200" class="blue font_23" >New Password</td>
                <td width="440">&nbsp;</td>
              </tr>
              <tr>
                <td class="blue font_18">Password *</td>
                <td><div class= "left-input"><div class= "right-input"><div class= "fill-input">
	       <input value="" type="password" name="password" id="pass1" />
        </div></div></div></td>
              </tr>
              <tr>
                <td class="blue font_18">Password confirmation*</td>
                <td><div class= "left-input"><div class= "right-input"><div class= "fill-input">
	       <input value="" type="password" name="password2" id="pass2" onkeyup="checkPass(); return false;"/>
        </div></div></div></td>
              </tr>
              <tr>
                <td><input type="button" value="" class="change_pass_button" onClick="if(checkPass()) { this.form.submit(); }" /></td>
                <td><div id="confirmMessage"></div></td>
              </tr>
            </table>                    
			<div class="closeEditButtonPass ">
                            	<a class="jqmClose" href="#"><img src="img/close_button.png" width="23" height="23" alt="Close" /></a>
             </div><!-- closeEditButton-->
	</div><!-- popup_wrap-->
</form>
<!-- -------------------------------------->               
          
	  </div><!-- contentClient-->
		</div><!-- #container-->
		
  </div><!-- #middle-->

<? require_once('footer.php'); ?>


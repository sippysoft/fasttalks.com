<?
$title = "Privacy Policy";

require_once('header.php'); ?>
	<div id="middle">

		<div id="container">
			<div id="contentHowItWorks">
				<div class="howItWorks_txt blue font_26">
                	Privacy Policy
                </div><!-- howItWorks_txt-->
                <div class="steps text_justify">
<p>
FastTalks is committed to protecting your privacy. We recognize the need to safeguard the personal information we collect from You in order for You to use the Service. 
</p>
<p>
FastTalks collects personal information when users register to use the Service. The personal information collected may consist of a unique access ID and contact information such as name, email address, phone and fax numbers, birth date, gender, zip code, occupation, industry, and personal interests. 
</p>
<p>
We may ask for further optional information from time to time to get a better picture of our visitors and their preferences. FastTalks encourages users to provide this information to ensure that the Service remains relevant to your interests and needs. 
</p>
<p>
<b>Information Sharing and Disclosure </b>
</p>
<p>
FastTalks may provide your information to trusted partners who work under confidentiality agreements. These companies may use your personal information to help FastTalks communicate with you about our offers and those from our marketing partners. These companies do not have any independent right to share this information. 
</p>
<p>
We believe it is necessary to share information in order to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of FastTalks' terms of use, or as otherwise required by law. 
</p>
<p>
We will transfer your information if FastTalks is acquired by or merged with another company. In this event, You will be notified before this information is transferred and becomes subject to a different privacy policy. 
</p>
<p>
<b>IP addresses </b>
</p>
<p>
IP addresses are used by your computer every time you are connected to the Internet. Your IP address is a number that is used by computers on the network to identify your computer. 
</p>
<p>
FastTalks uses IP addresses to analyse trends, administer the site, track user?s movement, and gather broad demographic information for aggregate use. IP addresses are not linked to personally identifiable information. Additionally, for systems administration, detecting usage patterns and troubleshooting purposes, our web servers automatically log standard access information including browser type, access times/open mail, URL requested, and referral URL. This information is not shared with third parties and is used by FastTalks on a need-to-know basis only. Any individually identifiable information related to this data will never be used in any way different to that stated above without your explicit permission.
</p>
<p>
<b>Confidentiality and Security </b>
</p>
<p>
Authorized employees within the company use any information collected from individual customers on a need-to-know basis only. We constantly review our systems and data to ensure the best possible service to our customers. We have physical, electronic, and procedural safeguards that comply with federal regulations to protect Your personal information. 
</p>
<p>
Your credit card details are handled safely and securely by authorize.net, one of the most trusted payment gateways in the world.
</p>
<p>
If you have any questions about Our Privacy Policy, contact <a href="mailto:support@fasttalks.com">support@fasttalks.com</a>.  
</p>
                </div><!-- steps-->                            	
            <div class="clearfix"></div>    
		  </div><!-- #content-->
		</div><!-- #container-->       

	</div><!-- #middle-->
<? require_once('footer.php'); ?>

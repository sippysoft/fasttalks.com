<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Sign Up</title>
	<meta name="title" content="" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    <!--[if lt IE 7]>
     <script defer type="text/javascript" src="script/pngfix.js"></script>
     <![endif]-->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" type="text/css" href="script/cusel.css" />
    <script type="text/javascript" src="script/jquery-1.4.2.js"></script>
    <script type="text/javascript" src="script/cusel-min-2.3.1.js"></script>
    <script type="text/javascript" src="script/select-settings.js"></script>

<script type= "text/javascript">
var RecaptchaOptions = {
theme: 'clean'
};
</script>

    </head>

<body>

<?php
  require_once('packages.php');
  require_once('recaptchalib.php');
  $privatekey = "ENTER_PRIVATE_KEY_HERE";

require_once('html.php');
require_once('xmlrpc.inc');
    

$currencies = array(
    'USD' => 'USD'
 );


$translate_errors = array(
'"Web Password" field should contain at least 7 symbols.' => 'Password should contain at least 7 symbols.',
'"Web Password" field is too long. Not more than 32 symbols is allowed.' => 'Password is too long. Not more than 32 symbols is allowed.',
'"Web Password" field should contain letters.' => 'Password should contain letters.',
'"Web Password" field should contain digits.' => 'Password should contain digits.',
'"Web Password" field should contain letters and digits.' => 'Password field should contain letters and digits.',
'Cannot add Account. Another account with conflicting "Account Name" or "VoIP Login" already exists.' => 'Please choose another Username. This one is already taken by another user.',
'Cannot add Account. Entered "Account Name" is not allowed.' => 'Such Username is not allowed. Please choose another Username',
'Wrong vm_notify_emails' => 'Wrong Email format. Please enter valid email.',
'Wrong vm_forward_emails' => 'Wrong Email format. Please enter valid email.',
'Wrong email' => 'Wrong Email format. Please enter valid email.',
'Wrong i_time_zone' => 'Please specify your Timezone/location.',
'incorrect-captcha-sol' => 'Incorrect text enetered. Please check captcha carefully.'
);



function createAccount() {
    global $reg_error;
    $params = array(new xmlrpcval(array("username"      => new xmlrpcval(get_par('username'), "string"),
                                    "web_password"      => new xmlrpcval(get_par('password'), "string"),
                                    "authname"          => new xmlrpcval(get_par('username'), "string"),
                                    "voip_password"     => new xmlrpcval('DUMMY_PASSWORD', "string"),
                                    "i_billing_plan"    => new xmlrpcval(get_par('i_billing_plan'), "int"),                             /* replace with your i_tariff */
                                    "i_time_zone"       => new xmlrpcval(get_par('i_time_zone'), "int"),
                                    "i_lang"            => new xmlrpcval("en", "string"),
                                    "balance"           => new xmlrpcval("0.0", "double"),                         /* set needed value */
                                    "credit_limit"      => new xmlrpcval("0.0", "double"),                         /* set needed value */
                                    "blocked"           => new xmlrpcval("", "int"),
                                    "max_sessions"      => new xmlrpcval("", "int"),
                                    "max_credit_time"   => new xmlrpcval("3600", "int"),
                                    "translation_rule"  => new xmlrpcval("", "string"),
                                    "cli_translation_rule"      => new xmlrpcval("", "string"),
                                    "cpe_number"        => new xmlrpcval("", "string"),
                                    "i_export_type"     => new xmlrpcval("2", "int"),
                                    "reg_allowed"       => new xmlrpcval("1", "int"),
                                    "trust_cli"         => new xmlrpcval("", "int"),
                                    "disallow_loops"    => new xmlrpcval("", "int"),
                                    "vm_password"       => new xmlrpcval('${N:[0-9][0-9][0-9][0-9][0-9][0-9]}', "string"),
                                    "vm_enabled"        => new xmlrpcval("1", "int"),
                                    "vm_notify_emails"  => new xmlrpcval(get_par('email'), "string"),
                                    "vm_forward_emails" => new xmlrpcval(get_par('email'), "string"),
                                    "vm_del_after_fwd"  => new xmlrpcval("", "int"),
                                    "company_name"      => new xmlrpcval(get_par('company_name'), "string"),
                                    "salutation"        => new xmlrpcval("Mr.", "string"),
                                    "first_name"        => new xmlrpcval('', "string"),
				    "mid_init"          => new xmlrpcval("", "string"),
				    "first_name"         => new xmlrpcval(get_par('first_name'), "string"),
                                    "last_name"         => new xmlrpcval(get_par('last_name'), "string"),
                                    "street_addr"       => new xmlrpcval(get_par('address'), "string"),
                                    "state"             => new xmlrpcval('', "string"),
                                    "postal_code"       => new xmlrpcval('', "string"),
                                    "city"              => new xmlrpcval('', "string"),
                                    "country"           => new xmlrpcval('', "string"),
                                    "contact"           => new xmlrpcval("", "string"),
                                    "phone"             => new xmlrpcval(get_par('phone'), "string"),
                                    "fax"               => new xmlrpcval('', "string"),
                                    "alt_phone"         => new xmlrpcval('', "string"),
                                    "alt_contact"       => new xmlrpcval("", "string"),
                                    "email"             => new xmlrpcval(get_par('email'), "string"),
                                    "cc"                => new xmlrpcval('', "string"),
                                    "bcc"               => new xmlrpcval('', "string"),
                                    "payment_currency"  => new xmlrpcval('USD', "string"),
                                    "payment_method"    => new xmlrpcval("1", "int"),
                                    "on_payment_action" => new xmlrpcval("", "int"),
                                    "min_payment_amount"=> new xmlrpcval("", "double"),
                                    "lifetime"          => new xmlrpcval("-1", "int"),
                                    "preferred_codec"   => new xmlrpcval("18", "int"),
                                    "use_preferred_codec_only"  => new xmlrpcval("", "int"),
                                    "i_media_relay_type"=> new xmlrpcval("1", "int"),
				    "i_password_policy" => new xmlrpcval("1", "int"),
				    "i_routing_group" => new xmlrpcval("3", "int"),
                                    "welcome_call_ivr"  => new xmlrpcval("", "int")
                                   ), 'struct'));
    $msg = new xmlrpcmsg('createAccount', $params);

    /* replace here URL  and credentials to access to the API */
    $cli = new xmlrpc_client('https://login.fasttalks.com/xmlapi/xmlapi');
    $cli->setSSLVerifyPeer(false);
    $cli->setCredentials(XML_LOGIN, XML_PASWORD, CURLAUTH_DIGEST);

    $r = $cli->send($msg, 20);       /* 20 seconds timeout */

    if ($r->faultCode()) {
      /* $re "Fault. Code: " . $r->faultCode() . ", Reason: " . $r->faultString(); */
      $reg_error = $r->faultString();
      return false;
    }

    return $r->value();
}


function sendEMail($a) {
    $body = "  Welcome to FastTalks!

  Please keep this email for your records. Your account information is as follows:

        Web Login: ${a['username']}
        Web Password: ${a['web_password']}

  Please do not forget your password as it has been encrypted in our database and
we cannot retrieve it for you. However, should you forget your password you can
request a password reset and a new one will be sent to you.

  To login to your account area please visit http://fasttalks.com/login.php

  When you login for the first time, the system will request a web login password change,
please make sure you change your password.

  To make Toll calls you will need to make on-line payments.

  Thank you for registering!

  FastTalks Team.";

$params = array(new xmlrpcval(array("from"      => new xmlrpcval('signup@fasttalks.com', "string"),
                                    "to"        => new xmlrpcval(get_par('email'), "string"),
                                    "cc"        => new xmlrpcval('', "string"),
                                    "bcc"       => new xmlrpcval('', "string"),
                                    "subject"   => new xmlrpcval('FastTalks Signup confirmation', "string"),
                                    "body"      => new xmlrpcval($body, "string")
                                   ), 'struct'));
    $msg = new xmlrpcmsg('sendEMail', $params);

    /* replace here URL  and credentials to access to the API */
    $cli = new xmlrpc_client('https://login.fasttalks.com/xmlapi/xmlapi');
    $cli->setSSLVerifyPeer(false);
    $cli->setCredentials(XML_LOGIN, XML_PASWORD, CURLAUTH_DIGEST);

    $r = $cli->send($msg, 20);       /* 20 seconds timeout */

    if ($r->faultCode()) {
      error_log("Fault. Code: " . $r->faultCode() . ", Reason: " . $r->faultString());
    }
}



$ok = true;


if(get_par("recaptcha_challenge_field")) {

      $resp = recaptcha_check_answer ($privatekey,
                    $_SERVER["REMOTE_ADDR"],
                    get_par("recaptcha_challenge_field"),
                    get_par("recaptcha_response_field"));
      if (!$resp->is_valid) {
        // What happens when the CAPTCHA was entered incorrectly
	$reg_error = $resp->error;
	$ok=false;      
      } else {
          $ok = true;
      }
}


if (isset_par('task')) {
    if (!get_par('email') || !get_par('username') || !get_par('password') || !get_par('last_name') || !get_par('i_time_zone')) {
       $reg_error = "Please fill all required fields"; 
       $ok = false;
    }
    if ($ok) {
        $r = createAccount();
        if ($r) {
            $a = Array();

            $v = $r->structmem('username');
            $a['username'] = $v->scalarval();

            $v = $r->structmem('web_password');
            $a['web_password'] = $v->scalarval();

            $v = $r->structmem('authname');
            $a['authname'] = $v->scalarval();

            $v = $r->structmem('voip_password');
            $a['voip_password'] = $v->scalarval();

            $v = $r->structmem('vm_password');
            $a['vm_password'] = $v->scalarval();

            sendEMail($a);
        } else {
            $ok = false;
        }
    }
} else {
    $par['i_time_zone'] = 88;          /* Europe/London */
    $par['payment_currency'] = 'USD';
}


   


$reg_error = strtr( $reg_error , $translate_errors);
$translate_errors = array(
	'"Web Password" field should contain at least 7 symbols.' => 'Password should contain at least 7 symbols.',
	'"Web Password" field is too long. Not more than 32 symbols is allowed.' => 'Password is too long. Not more than 32 symbols is allowed.',
	'"Web Password" field should contain letters.' => 'Password should contain letters.',
	'"Web Password" field should contain digits.' => 'Password should contain digits.',
	'"Web Password" field should contain letters and digits.' => 'Password field should contain letters and digits.',
	'Cannot add Account. Another account with conflicting "Account Name" or "VoIP Login" already exists.' => 'Please choose another Username. This one is already taken by another user.',
	'Cannot add Account. Entered "Account Name" is not allowed.' => 'Such Username is not allowed. Please choose another Username',
	'Wrong vm_notify_emails' => 'Wrong Email format. Please enter valid email.',
	'Wrong vm_forward_emails' => 'Wrong Email format. Please enter valid email.',
	'Wrong email' => 'Wrong Email format. Please enter valid email.',
	'Wrong i_time_zone' => 'Please specify your Timezone/location.',
	'incorrect-captcha-sol' => 'Incorrect text enetered. Please check captcha carefully.'
	);

$highlight_username = 0;
$highlight_password = 0;
$highlight_email = 0;

$highlight_username = 0;
if(preg_match("/Username/",$reg_error)) {
	$highlight_username = 1;
}
if(preg_match("/Password/",$reg_error)) {
	$highlight_password = 1;
}
if(preg_match("/Email/",$reg_error)) {
	$highlight_email = 1;
}



  ?>

<div id="wrapper">

	<div id="header">
    	<div class="logo">
        	<a href="index.php"><img src="img/logo.jpg" title="FastTalks.com" width="178" height="104" alt="FastTalks.com" /></a>
      </div><!-- .logo-->
        <div class="login">
        	<ul>
            	<li><a href="sign-up.php"><img src="img/singUp.png" width="112" height="37" alt="Sing up" /></a></li>	
                <li><a href="login.php"><img src="img/login.png" alt="" width="112" height="37" /></a></li>
            </ul>
        </div><!-- login-->
  <div class="menu">
        	<ul>
            	<li><a  href="index.php">Home</a></li>
                <li><a href="pricing.php">Pricing</a></li>
                <li><a href="how-it-works.php">How It Works</a></li>
                <li class="last"><a href="about-us.php">About Us</a></li>                
            </ul>
        </div><!-- .menu-->        
      <div class="clearfix"></div>
        
	</div><!-- #header-->
    
	<div id="middle">


<form action=sign-up.php method=post>
<input type=hidden name=task value=1>
	  <div id="contentClient">
            <div class="profile_edit_txt">
            	<span class="font_26 blue">Sign Up</span>  

            </div><!-- profile_edit_txt-->	
<div class="profileContent">  
                <table width="620" border="0" cellspacing="0" cellpadding="0">

<?
if (isset_par('task') && $ok) { ?>
  <tr>
    <td colspan="3" width=100%>
    	Thank you for registering. Your account information and access details have been sent to <?php echo get_par('email'); ?>	
    </td>
  </tr>      
<? } else { 

if($reg_error) {


echo "<tr>
    <td width=\"226\" ><span class=\"red\">Signing up problem: </span> </td>
    <td colspan=\"2\" class=\"blue\">

<span class=\"red\">$reg_error</span>
		

    </td>
  </tr>";

}

?>



  <tr class="forgotPassword" >
    <td width="226"  >User name <span class="red">*</span>: </td>
    <td colspan="2" class="blue">
    	<div class= "left-input <?   if($highlight_username) echo "false_input_left"; ?>">
	<div class= "right-input <?   if($highlight_username) echo "false_input_right"; ?>">
	<div class="fill-input  <?   if($highlight_username) echo "false_input_mid"; ?>">
	          <input name=username value="<?php echo get_par('username'); ?>" type="text" />
	    </div></div></div>
    </td>
  </tr>


  <script type="text/javascript">
    function checkPass(){
      //Store the password field objects into variables ...
      var pass1 = document.getElementById('pass1');
      var pass2 = document.getElementById('pass2');
      //Store the Confimation Message Object ...
      var message = document.getElementById('confirmMessage');
      //Set the colors we will be using ...
      var goodColor = "#66cc66";
      var badColor = "#ff6666";
      //Compare the values in the password field
      //and the confirmation field
      if(pass1.value == pass2.value){
        //The passwords match.
        //Set the color to the good color and inform
        //the user that they have entered the correct password
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
        return true;
      }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
        return false;
      }
    }
  </script>


  <tr  class="forgotPassword" >
    <td>Password <span class="red">*</span>:</td>
    <td colspan="2" class="blue">
	<div class= "left-input <?   if($highlight_password) echo "false_input_left"; ?>">
        <div class= "right-input <?   if($highlight_password) echo "false_input_right"; ?>">
        <div class="fill-input  <?   if($highlight_password) echo "false_input_mid"; ?>">

	          <input name="password" id="pass1" value="<?php echo get_par('password'); ?>" type="password" />
            </div></div></div>
    
    </td>
  </tr>

  <tr  class="forgotPassword" >
    <td>Confirm Password<span class="red">*</span>:</td>
    <td colspan="2" class="blue">
        <div class= "left-input <?   if($highlight_password) echo "false_input_left"; ?>">
        <div class= "right-input <?   if($highlight_password) echo "false_input_right"; ?>">
        <div class="fill-input  <?   if($highlight_password) echo "false_input_mid"; ?>">

                  <input name="password2" id="pass2" value="<?php echo get_par('password'); ?>" type="password" onkeyup="checkPass(); return false;"  />
            </div></div></div>

    </td>
  </tr>
<tr>
    <td></td>
    <td colspan="2" class="blue">
	<div id="confirmMessage"></div>
    </td>
</tr>


  <tr  class="forgotPassword" >
    <td>Email <span class="red">*</span>: </td>
    <td colspan="2">
	<div class= "left-input <?   if($highlight_username) echo "false_input_left"; ?>">
        <div class= "right-input <?   if($highlight_username) echo "false_input_right"; ?>">
        <div class="fill-input  <?   if($highlight_username) echo "false_input_mid"; ?>">

	          <input name="email" value="<?php echo get_par('email'); ?>" type="text" />
            </div></div></div>
    
    </td>
  </tr>
	

  <tr  class="forgotPassword" >
    <td>First Name <span class="red">*</span>:</td>
    <td colspan="2" class="blue">
        <div class= "left-input"><div class= "right-input"><div class= "fill-input">
               <input name="first_name" value="<?php echo get_par('first_name'); ?>" type="text" />
        </div></div></div>
    </td>
  </tr>


  <tr  class="forgotPassword" >
    <td>Last Name <span class="red">*</span>:</td>
    <td colspan="2" class="blue">
        <div class= "left-input"><div class= "right-input"><div class= "fill-input">
	       <input name="last_name" value="<?php echo get_par('last_name'); ?>" type="text" />
        </div></div></div>    
    </td>
  </tr>
  <tr  class="forgotPassword" >
    <td>Time Zone <span class="red">*</span>:</td>
    <td colspan="2" class="blue">
      <div class="lineForm">       
		<select class="sel80" id="timeZone" name="i_time_zone">
<?php
    $i_time_zone = (int) get_par('i_time_zone');
    if($i_time_zone ==0)  $i_time_zone = 88;

    foreach ($tzs as $k => $v) {
        $selected = ( $i_time_zone == $k) ? ' selected' : '';
        print "<option value=\"$k\" label=\"$v\"$selected>$v</option>\n";
    }
?>		</select>                    
    </div>	  
    
    </td>
  </tr>
  <tr  class="forgotPassword" >
    <td >Selected Package <span class="red">*</span>:</td>
    <td colspan="2" class="blue">
    		<div class="lineForm">	        
		<select name="i_billing_plan" class="sel80" id="package" >
<?php  foreach ($packages as $k => $package) {
        $selected = get_par('i_billing_plan') == $package['i_billing_plan'] ? ' selected' : '';
        print "<option value=\"".$package['i_billing_plan']."\" label=\"".$package['name']."\" $selected>".$package['name']."</option>\n";
    }
?>
		</select>                  
    </div>	  
    </td>
  </tr>
  <tr >
    <td class="forgotPassword" >    
    Company Name: </td>
    <td colspan="2" class="blue">
    		<div class= "left-input"><div class= "right-input"><div class= "fill-input">
	          <input name="company_name" value="<?php echo get_par('company_name'); ?>" type="text" />
            </div></div></div>
    </td>
  </tr>
  <tr>
    <td class="forgotPassword">Address</td>
    <td colspan="2" align="left"> 
      <div class="left-input"><div class= "right-input"><div class= "fill-input">
        <input name="address" value="<?php echo get_par('address'); ?>" type="text" />
      </div></div></div></td>
    </tr>
  <tr>
    <td class="forgotPassword">Phone</td>
    <td colspan="2" align="left"> <div class="left-input"><div class= "right-input"><div class= "fill-input">
      <input name="phone" value="<?php echo get_par('phone'); ?>" type="text" />
    </div></div></div></td>
    </tr>


  <tr >
    <td class="forgotPassword">Are You Human?</td>
    <td colspan="2" align="left" valign="top">

   <?php
          require_once('recaptchalib.php');
          $publickey = "6LcNeMISAAAAAHLql6zQaWH5_cBvmSOxwohN_PFU"; // you got this from the signup page
          echo recaptcha_get_html($publickey);
        ?>
    </td>

    </tr>


  <tr>
    <td colspan="3" valign="middle"><label>
<font size="-1">
  By clicking "Sign Up" button, you are indicating that you have read and agree to <br>the <a href="terms-and-conditions.php" >Terms of Use</a> and <a href="privacy-policy.php" >Privacy Policy</a>.
  </font>
  </label></td>
    </tr>
  <tr>
    <td>&nbsp; </td>
    <td width="205" align="center">&nbsp;</td>
    <td width="219" align="right"><input type="image" src="img/singUp.png" onClick="if(checkPass()) { this.form.submit(); }"          /></td>
  </tr>

<?php } ?>
</table>
</form>
              	
                </div><!-- profileContent-->               
                
                
	  </div><!-- contentClient-->
		</div><!-- #container-->
		
  </div><!-- #middle-->

	<div id="footer">
    	<div class="copyright blue">
        	Copyright 2011 FastTalks, Inc.
        </div><!-- copyright-->
    	<div class="bottomMenu">
        	<ul>
            	<li><a href="privacy-policy.php">Privacy Policy</a></li>
                <li><a href="terms-and-conditions.php">Terms &amp; Conditions</a></li>
                <li><a href="refund-policy.php">Refund Policy</a></li>
                <li><a href="contact-us.php">Contact Us</a></li>
            </ul>
        </div><!-- bottomMenu-->
    </div><!-- footer-->
<!-- #wrapper -->

</body>
</html>

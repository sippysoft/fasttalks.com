<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Fast Talks</title>
	<meta name="title" content="" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    <!--[if lt IE 7]>
    <script defer type="text/javascript" src="script/pngfix.js"></script>
    <![endif]-->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="screen, projection" />
</head>

<body>

<div id="wrapper">
	<div id="header">
    	<div class="logo">
        	<a href="index.php"><img src="img/logo.jpg" title="FastTalks.com" width="178" height="104" alt="FastTalks.com" /></a>
        </div><!-- .logo-->
        <div class="control_panel">
	        <ul>
            	<li class="blue">Homer Simpson</li>	
                <li><a href="information.php"><img src="img/control_panel.png" width="153" height="37" alt="Control Panel" /></a></li>
                <li><a class="orange" href="#">Logout</a></li>                
            </ul>
        </div><!-- .control_panel-->
        <div class="login">
        	<ul>
            	<li><a href="sign-up.php"><img src="img/singUp.png" width="112" height="37" alt="Sing up" /></a></li>	
                <li><a href="login.php"><img src="img/login.png" alt="" width="112" height="37" /></a></li>
            </ul>
        </div><!-- login-->
  <div class="menu">
        	<ul>
            	<li class="active"><a  href="index.php">Home</a></li>
                <li><a href="pricing.php">Pricing</a></li>
                <li><a href="how-it-works.php">How It Works</a></li>
                <li class="last"><a href="about-us.php">About Us</a></li>                
            </ul>
        </div><!-- .menu--> 
     </div><!-- #header-->       
      <div class="clearfix"></div>
        <div class="ourPartners">        	
       
       	  <div class="txt_GetCalls">Receive calls from your website</div><!-- txt_callUs-->
          <div class="txt_justOneClick">
            <p>FastTalks' "<span class="bold">Call Us</span>" service makes it easy for your web site visitors to call you from their browser</p>

            <p class="font_23 bold"><span class="red">Just in one click</span> - <span class="blue">without a phone.</span></p></div><!-- justOneClick-->
       
        
        	<div class="ourPartners_txt">Our Partners</div>           
        	<div class="partnersLinks">
            	<div class="partnersLinks_col_1">
                	<ul>
	                    <li><a href="#"><img src="img/link_parntners_2.jpg" alt="" width="73" height="27" /></a></li>
                        <li><a href="#"><img src="img/link_parntners_3.jpg" alt="" width="81" height="31" /></a></li>
                        <li><a href="#"><img src="img/link_parntners_4.jpg" alt="" width="74" height="29" /></a></li>
                        <li><a href="#"><img src="img/link_parntners_5.jpg" alt="" width="74" height="33" /></a></li>
                    </ul>
                </div><!-- partnersLinks_col_1-->
                <div class="partnersLinks_col_2">
					<ul>
	                    <li><a href="#"><img src="img/link_parntners_6.jpg" alt="" width="79" height="35" /></a></li>
                        <li><a href="#"><img src="img/link_parntners_7.jpg" alt="" width="94" height="26" /></a></li>
                        <li><a href="#"><img src="img/link_parntners_2.jpg" alt="" width="73" height="27" /></a></li>
                        <li><a href="#"><img src="img/link_parntners_3.jpg" alt="" width="81" height="31" /></a></li>
                    </ul>
                </div><!-- partnersLinks_col_2-->
            </div><!-- partnersLinks-->            
        
            
        </div><!-- .ourPartners-->        
	<div class="clearfix"></div>		
	

	<div id="middle">
<!--[if lte IE 6]>
<style type="text/css">
.ie_padding {height: 20px; display: block}
.row_1_ie {height: 37px !important; }
.row_2_ie {height:81px !important;  }
.row_3_ie {height:34px !important; }
.row_4_ie {height:25px !important; }
.row_56_ie {height:30px !important; }
.row_7_ie {height: 42px !important; }
.iefix { margin: 10px 0 0 0 ; display: block}
</style>
<![endif]-->

		<div id="container">
			<div id="content" class="margin_top_25">
			
            <div class="buttonsWrap">
        			<div class="singUpNow">
            			<a href="sign-up.php"><img src="img/singUpNow.png" width="196" height="45" alt="Sing Up Now" /></a>
              </div><!-- singUpNow-->
		            <div class="howItWorks">
        		    	<a href="how-it-works.php"><img src="img/howItWorks.gif" width="238" height="45" alt="How it works" /></a>
	               </div><!-- howItWorks-->
		       </div>
            
			<div class="callUsService_left">
                	<div class="callUsService_right">
                    	<div class="callUsService">
                			<img align="left" src="img/callus.jpg" width="123" height="84" alt="Call Us Service" />
                    <p>
						If your website visitors want questions answered, they want them answered quickly . whether you.re a one-man startup or a flourishing e-business. Make it easy for them with FastTalks. <b>Call Us</b> service. 
						<br><br>Call Us enables <b>hassle-free voice calls</b> to be made directly through your website . <b>no phone or download required</b> . so potential clients are just <b>one mouse click away from your direct line</b>. 
						<br><br>All you need to do is embed a Call Us button on your site. It.s the <b>simple, effective</b> way for your website visitors to get the answers they need.
					</p>
                		</div><!-- callUsService-->
                    </div><!-- callUsService_right-->
                </div><!-- callUsService_left-->
			
                <div class="table_wrap">
                              <table class="prices_table"  border="0" cellspacing="0" cellpadding="0">
                  <tr >
                    <td width="234" class="row_1_ie	"  valign="middle"  ><span class="pad_left_40"><b>Package</b></span></td>
                    <td width="125" align="center" ><b class="blue">Personal</b></td>
                    <td width="115" align="center" ><b class="orange">Company</b></td>
                    <td width="149"  align="center" ><b class="red">Enterprise</b></td>
                  </tr>
                  <tr>
                    <td   class="row_2_ie" >&nbsp;</td>
                    <td align="center" valign="middle"><b>$29</b> / month<br />
                      <br /><a href="sign-up.php?i_billing_plan=11"><img src="img/singUpTable_blue.png" alt="sing up" width="101" height="20" /></a></td>
                    <td align="center" valign="middle"><b>$59</b> / month<br />
                      <br /><a href="sign-up.php?i_billing_plan=12"><img src="img/singUpTable_orange.png" width="101" height="20" alt="sing up" /></a></td>
                    <td  align="center" valign="middle"><b>$199</b> / month<br />
                      <br /><a href="sign-up.php?i_billing_plan=13"><img src="img/singUpTable_red.png" width="102" height="18" alt="sing up" /></a></td>
                  </tr>
                  <tr>
                    <td  class="row_3_ie pad_left_40"><b>Minutes included *</b><br /></td>
                    <td align="center" valign="middle">600</td>
                    <td  align="center" valign="middle">1200</td>
                    <td align="center" valign="middle">5000</td>
                  </tr>
<!---                  <tr>
                    <td  class="row_3_ie pad_left_40"><b>Additional minutes *</b><br /></td>
                    <td align="center" valign="middle">5.</td>
                    <td  align="center" valign="middle">5.</td>
                    <td align="center" valign="middle">4.</td>
                  </tr>
                  <tr>
                    <td class="row_4_ie" ><span class="pad_left_40">Call Encryption</span></td>
                    <td align="center"><img src="img/round.png" alt="" width="7" height="7" /></td>
                    <td align="center"><img src="img/round.png" alt="" width="7" height="7" /></td>
                    <td align="center"><img src="img/round.png" alt="" width="7" height="7" /></td>
                  </tr>
                  <tr>
                    <td class="row_5_ie" ><span class="pad_left_40"><b>E-mail</b> support</span></td>
                    <td align="center"><img src="img/round.png" alt="" width="7" height="7" /></td>
                    <td align="center"><img src="img/round.png" alt="" width="7" height="7" /></td>
                    <td align="center"><img src="img/round.png" alt="" width="7" height="7" /></td>
                  </tr>  --->
                    <td class="row_4_ie" ><span class="pad_left_40">Voicemail</span></td>
                    <td align="center">-</td>
                    <td align="center"><img src="img/round.png" alt="" width="7" height="7" /></td>
                    <td align="center"><img src="img/round.png" alt="" width="7" height="7" /></td>
                  </tr>
                  <tr >
                    <td  class="row_56_ie "><b class="pad_left_40">Call</b> recording</td>
                    <td align="center">-</td>
                    <td align="center"><img src="img/round.png" alt="" width="7" height="7" /></td>
                    <td align="center"><img src="img/round.png" alt="" width="7" height="7" /></td>
                  </tr>
                  <tr >
                    <td  class="row_56_ie "><b class="pad_left_40">Priority</b> support</td>
                    <td align="center">Working hours</td>
                    <td align="center">Working hours</td>
                    <td align="center">24*7</td>
                  </tr>
<!---                  <tr>
                    <td class="row_56_ie"><span class="pad_left_40"><b>Phone</b> support</span></td>
                    <td align="center">-</td>
                    <td align="center">-</td>
                    <td align="center"><img src="img/round.png" alt="" width="7" height="7" /></td>
                  </tr>  --->
                  <tr>
                    <td valign="middle" class="row_7 "><a  class="blue font_18 pad_left_40" href="pricing.php"><b class="iefix">More details</b></a></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                </table>
                </div>
				<p><b>*</b> The amount of minutes included and additional minutes costs are applied for the USA and Canada. These figures are subject to recalculation for other countries.</p>
                
		  </div><!-- #content-->
		</div><!-- #container-->
		
        <div class="sidebar margin_top_25" id="sideRight">

                    <div class="wrapper_content">
                        <div class="inner">
                            <div class="content-bg"></div>
                                <div class="content_text">
                                	<div class="main_text">
                                       One morning I was freaking out and almost in tears because 
                                       I couldn't keep up with the phone calls. It was insane. Ifbyphone came to our rescue.<br />
								    	<i>Tara Staglik, Papa Dean's Popcorn</i>
                                    </div><!-- main_text-->								
                                </div><!-- .content_text-->
                            </div><!-- .content-bg-->
                        </div><!-- .inner-->


				
                    <div class="wrapper_content">
                        <div class="inner">
                            <div class="content-bg"></div>
                                <div class="content_text">
                                	<div class="main_text">
                                       One morning I was freaking out and almost in tears because 
                                       I couldn't keep up with the phone calls. It was insane. Ifbyphone came to our rescue.<br />
									   <i>Tara Staglik, Papa Dean's Popcorn </i>
                    				</div><!-- main_text-->								
                                </div><!-- .content_text-->
                            </div><!-- .content-bg-->
                        </div><!-- .inner-->


			        <div class="wrapper_content">
                        <div class="inner">
                            <div class="content-bg"></div>
                                <div class="content_text">
                                	<div class="main_text">
                                       One morning I was freaking out and almost in tears because 
                                       I couldn't keep up with the phone calls. It was insane. Ifbyphone came to our rescue.<br />
									   <i>Tara Staglik, Papa Dean's Popcorn </i>
                    				</div><!-- main_text-->								
                                </div><!-- .content_text-->
                            </div><!-- .content-bg-->
                        </div><!-- .inner-->
      


		</div><!-- .sidebar#sideRight -->		

	</div><!-- #middle-->

	<div id="footer">
    	<div class="copyright blue">
        	Copyright 2011 FastTalks, Inc.
        </div><!-- copyright-->
    	<div class="bottomMenu">
        	<ul>
            	<li><a href="privacy-policy.php">Privacy Policy</a></li>
                <li><a href="terms-and-conditions.php">Terms &amp; Conditions</a></li>
                <li><a href="refund-policy.php">Refund Policy</a></li>
                <li><a href="contact-us.php">Contact Us</a></li>
            </ul>
        </div><!-- bottomMenu-->
    </div><!-- footer-->
</div><!-- #wrapper -->

</body>
</html>

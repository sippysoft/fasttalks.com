<? 
$title = "About Us";
require_once('header.php'); ?>
<div id="middle">



		<div id="container">

			<div id="contentHowItWorks">

				<div class="howItWorks_txt blue font_26">

                	About Us

                </div><!-- howItWorks_txt-->

                <div class="steps text_justify">

					<b>FastTalks Inc.</b> is a subsidiary of <b>Sippy Software, Inc.</b> (<a href="http://sippysoft.com">http://sippysoft.com</a>), a leading developer of next-generation technology for providers of internet telephony (Voice over Internet Protocol, <b>VoIP</b>). 

					<br><br>Sippy.s high-performance and cost-effective technology powers hundreds of varied VoIP providers in more than 50 countries around the world. The company also offers a full range of professional services to meet specific client needs, including consulting, technical support and custom software development. 

					<br><br>Sippy Software, Inc. was founded in 2004 and is a privately held Canadian corporation, with offices in Canada, the United States and Europe. 

					<br><br>FastTalks Inc. was incorporated in Canada in 2010 and is focused on improving business-to-customer communications by means of the VoIP <b>Call Us</b> service. 

					<br><br> &nbsp;



					<p><b>FastTalks, Inc.</b>

						<br>506 - 210 Eleventh Street

						<br>New Westminster, BC V3M 4C9

						<br>Canada



						<br><br>Sales: <a href="mailto:sales@fasttalks.com">sales@fasttalks.com</a>

						<br>Technical Support: <a href="mailto:sales@fasttalks.com">support@fasttalks.com</a>

						<br>Billing: <a href="mailto:sales@fasttalks.com">billing@fasttalks.com</a>

						<br>General Inquires: <a href="mailto:sales@fasttalks.com">info@fasttalks.com</a>



						<br><br>Tel/Fax: +1 646-651-1110

					</p>

                </div><!-- steps-->                            	

            <div class="clearfix"></div>    

		  </div><!-- #content-->

		</div><!-- #container-->       



	</div><!-- #middle-->



<? require_once('footer.php'); ?>

<?
session_start();
$fail=0;
require_once('mysql.php');
require_once('html.php');
$update_error="";

if(isset($_SESSION['i_account'])) {
	$i_account = $_SESSION['i_account'];
	if(!$i_account)  {
		$fail =1;
	} else {
		if(get_par('update')) {
			$update_error.=validate_input(get_par('name'), 'alphanumeric', 'Button Name');
			$update_error.=validate_input(get_par('numbers'), 'numbers', 'Phone Numbers');
			$update_error.=validate_input(get_par('call_label'), 'alphanumeric', 'Call label');
                        $update_error.=validate_input(get_par('cancel_label'), 'alphanumeric', 'Cancel label');
                        $update_error.=validate_input(get_par('disconnect_label'), 'alphanumeric', 'Disconnect label');
                        $update_error.=validate_input(get_par('progress_label'), 'alphanumeric', 'Progress label');
                        $update_error.=validate_input(get_par('preset'), 'alphanumeric', 'Visual preset');
                        $update_error.=validate_input(get_par('widget_width'), 'digits', 'Widget width');
                        $update_error.=validate_input(get_par('widget_height'), 'digits', 'Widget height');
                        $update_error.=validate_input(get_par('button_width'), 'digits', 'Button widht');
                        $update_error.=validate_input(get_par('button_height'), 'digits', 'Button height');

			if(get_par('active')) $active=1; else $active=0;

			if($update_error == "") {
				$query="UPDATE 	buttons SET
						call_label = '".get_par('call_label')."',
						name = '".get_par('name')."',
                                                numbers = '".get_par('numbers')."',
                                                cancel_label = '".get_par('cancel_label')."',
                                                disconnect_label = '".get_par('disconnect_label')."',
                                                progress_label = '".get_par('progress_label')."',
                                                preset = '".get_par('preset')."',
                                                widget_width = '".get_par('widget_width')."',
                                                widget_height = '".get_par('widget_height')."',
                                                button_width = '".get_par('button_width')."',
						button_height = '".get_par('button_height')."',
						active = $active
					WHERE 	id = ".get_par('id')."
					AND 	i_account = $i_account";
				$res=mysql_query($query);

				header('Location: button-constructor.php');

				$query="SELECT  name, numbers, id,
                                	call_label, cancel_label, disconnect_label,
                                	progress_label, preset, widget_width,
					widget_height, button_width, button_height, 
					active
                        	FROM    buttons
                        	WHERE   i_account=$i_account
                        	AND     id=".get_par('id');

                		$res=mysql_query($query);
                		if($row=mysql_fetch_row($res) ) {
                        		$name           =$row[0];
                        		$numbers        =$row[1];
                        		$id             =$row[2];
                        		$call_label     =$row[3];
                        		$cancel_label   =$row[4];
                        		$disconnect_label=$row[5];
                        		$progress_label =$row[6];
                        		$preset         =$row[7];
                        		$widget_width   =$row[8];
                        		$widget_height  =$row[9];
                        		$button_width   =$row[10];
					$button_height  =$row[11];
					$active 	=$row[12];
                		} else {
                        		$fail=1;
                		}

			} else {
				$name           = get_par('name');
                        	$numbers        = get_par('numbers');
                        	$id             = get_par('id');
                        	$call_label     = get_par('call_label');
                        	$cancel_label   = get_par('cancel_label');
                        	$disconnect_label= get_par('disconnect_label');
                        	$progress_label = get_par('progress_label');
                        	$preset         = get_par('preset');
                        	$widget_width   = get_par('widget_width');
                	        $widget_height  = get_par('widget_height');
        	                $button_width   = get_par('button_width');
	                        $button_height  = get_par('button_height');
			}
			
		} else {
	                $query="SELECT  name, numbers, id,
                                        call_label, cancel_label, disconnect_label,
                                        progress_label, preset, widget_width,
					widget_height, button_width, button_height,
					active
                                FROM    buttons
                                WHERE   i_account=$i_account
                                AND     id=".get_par('id');
                        $res=mysql_query($query);
			if($row=mysql_fetch_row($res) ) {
                        	$name           =$row[0];
                                $numbers        =$row[1];
                                $id             =$row[2];
                                $call_label     =$row[3];
                                $cancel_label   =$row[4];
                                $disconnect_label=$row[5];
                                $progress_label =$row[6];
                                $preset         =$row[7];
                                $widget_width   =$row[8];
                                $widget_height  =$row[9];
                                $button_width   =$row[10];
				$button_height  =$row[11];
				$active 	=$row[12];
                    	} else {
                        	$fail=1;
                        }

		}
		
	}
} else {
	$fail=1;
}	
if($fail) {
                header('Location:login.php');
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Widget Designer</title>
	<meta name="title" content="" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    <!--[if lt IE 7]>
     <script defer type="text/javascript" src="script/pngfix.js"></script>
     <![endif]-->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" type="text/css" href="script/cusel.css" />
    <script type="text/javascript" src="script/jquery-1.4.2.js"></script>
    <script type="text/javascript" src="script/cusel-min-2.3.1.js"></script>
<script type="text/javascript" src="script/select-settings.js"></script>      


<script type="text/JavaScript">
function changeState(state)
{
	if(state==1){$('#callBtn').val($('#callInput').val());}
		if(state==2)
		{
			$('#establish').css('display','block');
			$('#callBtn').val($('#cancelInput').val());
		}
		else{$('#establish').css('display','none');}
			if(state==3){$('#callBtn').val($('#connectInput').val());}
}
</script>
</head>

<body>


<div id="wrapper">

	<div id="header">
    	<div class="logo">
        	<a href="index.php"><img src="img/logo.jpg" title="FastTalks.com" width="178" height="104" alt="FastTalks.com" /></a>
      </div><!-- .logo--><!-- login-->
        <div class="payments">
    	  	<div class="makePayment">
            	<a href="#"><img src="img/makePayment.png" width="101" height="24" alt="make payment" /></a>
	        </div><!-- makePayment-->
        	<div class="price">
           	  234$
    </div><!-- price-->
            <div class="logout">
            	<a class="orange" href="#">Logout</a>
            </div><!-- logout-->
            <div class="logout">
            </div><!-- logout-->
            <div class="balance blue bold">
	            Balance
            </div><!-- balance-->
        
      </div><!-- payments-->  
  <div class="menu_client">
        	<ul>
            	<li><a  class="menu_client_active" href="information.php">Information </a></li>
                <li class="large_bg_menu"><a href="button-constructor.php">&quot;Call Us&quot; button constructor</a></li>
                <li><a href="calls-history.php">Call History </a></li>
                <li class="last"><a href="payments-history.php">Payments History</a></li>                
            </ul>   
        </div><!-- .menu-->              

	<div class="clearfix"></div>
	</div><!-- #header-->
    
	<div id="middle">
	<form action="widget-design.php" method=POST>
                <input type=hidden name="update" id="update" value=1>
                <input type=hidden name="id"  id="id" value="<? echo $id;?>" >


	  <div id="contentClient">
	    <div class="profile_edit_txt">
		
		<span class="font_26 blue">Widget Design</span>  
            </div><!-- profile_edit_txt-->	
	<div class="profileContent">
    	<div class="button_design" id="widget">
       <table width="240" height="100%" border="0">
  <tr>
  <td align="center" valign="middle"><input  name="empty" style="color:#FFF; border:1px; background:#<? echo $preset; ?>; font-size:18px; width:<? echo $button_width; ?>px; height:<? echo $button_height; ?>px" type="button" value="<? echo $call_label; ?>" id="callBtn"/><!-- button_design--></td>
    </tr>
    <tr>
    <td align="center" valign="middle"><div style="display:none" id="establish"><? echo $progress_label; ?></div></td>
  </tr>
</table>

			
      	</div><!-- button_design-->
		<table border="0" width="630" cellspacing="0" cellpadding="0">

<? if($update_error != "") echo "<tr>
			<td width=\"190\"><font color='red'>Please fix:</font></td> 
			<td width=\"440\">$update_error</td>

				</tr>";?>
  <tr>
    <td width="190">Button Name</td>
    <td width="440">
	   	<div class= "left-input"><div class= "right-input"><div class= "fill-input">
		<input name="name" value="<? echo $name;?>" type="text" />
        </div></div></div>
    </td>
  </tr>
  <tr>
    <td>Phone Numbers</td>
    <td >
       	<div class= "left-input"><div class= "right-input"><div class= "fill-input">
	<input name="numbers" value="<? echo $numbers;?>" type="text" />
        </div></div></div>    
    </td>
  </tr>
  <tr>
    <td>Active</td>
    <td  align="left" class="padd_right_13"> <input type="checkbox" name="active" value="1" <? if($active) echo "checked";?> /></td>
  </tr>
  <tr>
    <td  ><span class="font_26 blue" > State </span></td>
    <td >&nbsp;</td>
  </tr>
  <tr >
    <td>Preview</td>
    <td  class="blue">
		     <div class="lineForm">
                    <select class="sel80" id="state" name="state"  onchange="changeState(this.value);" >
                    <option selected="selected" value="1">Intial state</option>
                    <option value="2">Connection</option>
                    <option value="3">Connected</option>                
		    </select>

            </div>	
    </td>
  </tr>
  <tr >
    <td><span class="font_26 blue" >Text settings</span></td>
    <td >&nbsp;</td>
  </tr>
  <tr >
    <td>Call label</td>
    <td  class="blue">
		    <div class= "left-input"><div class= "right-input"><div class= "fill-input">
		    <input name="call_label"  value="<? echo $call_label;?>" type="text" onkeyup="if($('#state').val()==1){$('#callBtn').val(this.value);}" id="callInput"/>
            </div></div></div>
    </td>
  </tr>
  <tr >
    <td>Cancel label</td>
    <td  class="blue">
		    <div class= "left-input"><div class= "right-input"><div class= "fill-input">
		    <input name="cancel_label"  value="<? echo $cancel_label;?>" type="text" id="cancelInput" onkeyup="if($('#state').val()==2){$('#callBtn').val(this.value);}"/>
            </div></div></div>
    </td>
  </tr>
  <tr >
    <td>Disconnect label</td>
    <td class="blue">
		    <div class= "left-input"><div class= "right-input"><div class= "fill-input">
		    <input name="disconnect_label"  value="<? echo $disconnect_label;?>" type="text" onkeyup="if($('#state').val()==3){$('#callBtn').val(this.value);}" id="connectInput"/>
            </div></div></div>
    </td>
  </tr>
  <tr >
    <td>Progress label</td>
    <td  class="blue">
		    <div class= "left-input"><div class= "right-input"><div class= "fill-input">
		    <input name="progress_label"  value="<? echo $progress_label;?>" type="text" onkeyup="$('#establish').html(this.value);"/>
            </div></div></div>    
    </td>
  </tr>
  <tr >
    <td><span class="font_26 blue" >Visual settings</span></td>
    <td class="blue">&nbsp;</td>
  </tr>
  <tr >
  <td>Visual preset</td>
    <td  class="blue">
    <div class="lineForm">
	<select class="sel80" id="preset_select"  name="preset"  onchange="$('#callBtn').css('background','#' + this.value);">
		<option <? if($preset=="ff7603" or strlen($preset)==0) echo "selected"; ?> value="ff7603" >Orange</option>
		<option <? if($preset=="000000") echo "selected"; ?> value="000000" >Black</option>
		<option <? if($preset=="0000AA") echo "selected"; ?> value="0000AA" >Dark blue</option>
		<option <? if($preset=="5555FF") echo "selected"; ?> value="5555FF" >Blue</option>
		<option <? if($preset=="00AA00") echo "selected"; ?> value="00AA00" >Green</option>
		<option <? if($preset=="AA0000") echo "selected"; ?> value="AA0000" >Red</option>
		<option <? if($preset=="FFFFFF") echo "selected"; ?> value="FFFFFF" >White</option>
		<option <? if($preset=="FFFF55") echo "selected"; ?> value="FFFF55" >Yellow</option>
		<option <? if($preset=="00AA00") echo "selected"; ?> value="00AA00" >Green</option>		
        </select>
    </div>	          
    </td>
  </tr>
  <tr >
    <td>Widget width</td>
    <td  class="blue">
		   <div class= "left-input"><div class= "right-input"><div class= "fill-input">
		   <input name="widget_width"  value="<? echo $widget_width;?>" type="text" onkeyup="$('#widget').css('width',this.value+'px');"/>
            </div></div></div>
    
    </td>
  </tr>
  <tr >
    <td>Widget height</td>
    <td class="blue">
    		<div class= "left-input"><div class= "right-input"><div class= "fill-input">
		<input name="widget_height"  value="<? echo $widget_height;?>" type="text"  onkeyup="$('#widget').css('height',this.value+'px');"/>
            </div></div></div>
    </td>
  </tr>
  <tr >
    <td>    
    Button width
    </td>
    <td  class="blue">
    		<div class= "left-input"><div class= "right-input"><div class= "fill-input">
		<input name="button_width"  value="<? echo $button_width;?>" type="text" onkeyup="$('#callBtn').css('width',this.value+'px');"/>
            </div></div></div>
    </td>
  </tr>
  <tr>
    <td>Button  height</td>
    <td>
		    <div class= "left-input"><div class= "right-input"><div class= "fill-input">
		    <input name="button_height"  value="<? echo $button_height;?>" type="text" onkeyup="$('#callBtn').css('height',this.value+'px');"/>
            </div></div></div>
    </td>
    
  </tr>
<!--
  <tr>
    <td class="blue"><input type="checkbox" /> Show DTMF keypad</td>
    <td align="right"><a  class="padd_right_13" href="#"><img src="img/show_advance_button.png" width="168" height="35" alt="show advance" /></a></td>
    
  </tr>
-->
  <tr>
    <td>&nbsp;</td>
    <td colspan="2" align="left">
 	<input class="return" type="button" value="" onClick="window.location='button-constructor.php';"/>
	<input class="saveWidget" value="" type="button" onClick="debugger;this.form.submit();"/>

    </td>
    </tr>
</table><br /><br />
                </div><!-- profileContent-->               
                
                
	  </div><!-- contentClient-->
		</div><!-- #container-->

</form>	
  </div><!-- #middle-->

	<div id="footer">
    	<div class="copyright blue">
        	Copyright 2011 FastTalks, Inc.
        </div><!-- copyright-->
    	<div class="bottomMenu">
        	<ul>
            	<li><a href="privacy-policy.php">Privacy Policy</a></li>
                <li><a href="terms-and-conditions.php">Terms &amp; Conditions</a></li>
                <li><a href="refund-policy.php">Refund Policy</a></li>
                <li><a href="contact-us.php">Contact Us</a></li>
            </ul>
        </div><!-- bottomMenu-->
    </div><!-- footer-->
<!-- #wrapper -->

</body>
</html>

<? 
$title = "Refund Policy";

require_once('header.php'); ?>
	<div id="middle">

		<div id="container">
			<div id="contentHowItWorks">
				<div class="howItWorks_txt blue font_26">
                	Refund Policy
                </div><!-- howItWorks_txt-->
                <div class="steps text_justify">
<p>				
				If for any reason you are not completely satisfied with the Service within 30 days of signup, you can receive a full refund on your initial package price. You may easily cancel the Service at any time. 
</p>
<p>
				You may submit a request for a refund by contacting FastTalks' customer services via e-mail at support@fasttalks.com. We reserve the right to deny repetitive refund requests. You may ask for a refund with 60 days of your payment. No refunds will be given for any charges which are more than 60 days old.
</p>
<p>
All refunds shall be paid either through the original payment method used, or any other reasonable payment method to be determined by FastTalks, and addressed to the person that deposited the credit initially. It may take up to 30 days to process refunds from date of your valid request date.
</p>
<p>
We will refund your credit balance in the event that we terminate this Agreement without serious cause. Any abuse by you of the terms relating to refunds hereunder shall lead to the termination of this Agreement.
</p>
                </div><!-- steps-->                            	
            <div class="clearfix"></div>    
		  </div><!-- #content-->
		</div><!-- #container-->       

	</div><!-- #middle-->

<? require_once('footer.php'); ?>
